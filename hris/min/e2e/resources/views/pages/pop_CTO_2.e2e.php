<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style>
         .border-left-bot {
            border-left: 1px solid black;
            border-bottom: 1px solid black;
         }
         .border-left-bot-right {
            border-left: 1px solid black;
            border-bottom: 1px solid black;
            border-right: 1px solid black;
         }
         .border-top {
            border-top: 1px solid black;
         }
         .border-left {
            border-left: 1px solid black;
         }
         .border-right {
            border-right: 1px solid black;
         }
         .gray {
            background: gray;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("");
         ?>
         <div class="txt-center bold" style="font-weight: 800; font-size: 18pt;">
            APPLICATION FOR AVAILMENT OF<br>COMPENSATORY TIME-OFF (CTO)
         </div>
         <div class="row margin-top">
            <div class="col-xs-12" style="padding: 10px;">
               <div class="row">
                  <div class="col-xs-6 border-left-bot border-top">
                     Name of Employee:
                     <br>
                     &nbsp;
                  </div>
                  <div class="col-xs-6 border-left-bot-right border-top">
                     Position:
                     <br>
                     &nbsp;
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-4 border-left-bot">
                     Office:
                     <br>
                     &nbsp;
                  </div>
                  <div class="col-xs-4 border-left-bot">
                     Division:
                     <br>
                     &nbsp;
                  </div>
                  <div class="col-xs-4 border-left-bot-right">
                     Date of Filing:
                     <br>
                     &nbsp;
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 gray text-center border-left border-right">
                     <b>
                        DETAILS OF APPLICATION
                     </b>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 border-left-bot text-center border-top">
                     Number of Hours Applied For
                     <br>
                     &nbsp;
                  </div>
                  <div class="col-xs-6 border-left-bot-right text-center border-top">
                     Inclusive Date/s
                     <br>
                     &nbsp;
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 border-left-bot">
                     <div class="row">
                        <div class="col-xs-12">
                           Requested by:
                           <br>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           _____________________________
                           <br>
                           Signature over name of applicant
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6 border-left-bot-right">
                     <div class="row">
                        <div class="col-xs-12">
                           Recommending Approval by:
                           <br>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           _____________________________
                           <br>
                           Immediate Supervisor
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12 gray text-center border-left border-right">
                     <b>
                        DETAILS OF ACTION APPLICATION
                     </b>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 border-left-bot border-top" style="height: 175px;">
                     <div class="row">
                        <div class="col-xs-12">
                           CERTIFICATION OF COMPENSATORY OVERTIME <br>CREDITS (COC) as of: ________________________
                           <br>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           Number of hours earned: _____________________________
                        </div>
                     </div>
                     <br>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           _____________________________
                           <br>
                           HR Officer
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6 border-left-bot-right border-top" style="height: 175px;">
                     <div class="row">
                        <div class="col-xs-12">
                           APPROVAL
                           <br>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-11">
                           <input type="checkbox" name="approved" id="approved" disabled>&nbsp; Approval
                           <br>
                           <input type="checkbox" name="disapproved" id="disapproved" disabled>&nbsp; Disapproval due to
                           <br>
                           __________________________________________
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           _____________________________
                           <br>
                           Head Of Office
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row border-left-bot-right">
                  <div class="col-xs-12">
                     1. The CTO may be availed of in the blocks of four (4) and eight (8) hours.
                     <br>
                     2. The employee may use the CTO continously up to a maximum five (5) consecutive days per single availment, or on
                     staggered basis within the year.
                     <br>
                     3. The employee must first obtain approval from the head of office regarding the schedule of availment of CTO.
                     <br>
                     4. Attach approved Certificate of COC Earned (prescribed form under Joint CSC-DBM Circular No. 2, series of 2004)
                     for validation purposes.
                  </div>
               </div>
            </div>
         </div>

         
      </div>
   </body>
</html>