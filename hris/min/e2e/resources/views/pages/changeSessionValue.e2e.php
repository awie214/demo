<?php
   session_start();
   include_once "constant.e2e.php";
   require_once pathClass.'0620functions.e2e.php';
   require_once 'conn.e2e.php';

   $gridTableHdr = getvalue("hGridTblHdr");
   $gridTableFld = getvalue("hGridTblFld");
   $gridTableId  = getvalue("hGridTblId");
   $gridDBTable  = getvalue("hGridDBTable");
   $sql          = getvalue("sql");
   $listAction   = getvalue("listAction");
   if (!empty(getvalue("json"))) {
      //echo getvalue("Module");
      $moduleContent = file_get_contents(json.getvalue("json").".json");
      $module        = json_decode($moduleContent, true);
      $listAction    = $module[getvalue("Module")]["Action"];
   } 
   $_SESSION["module_table"] = $gridDBTable;
   $_SESSION["module_sql"] = $sql;
   $_SESSION["list_ShowAction"] = $listAction;
   $_SESSION["module_gridTableHdr_arr"] = explode("|",$gridTableHdr);
   $_SESSION["module_gridTableFld_arr"] = explode("|",$gridTableFld);
   $_SESSION["module_gridTable_ID"] = $gridTableId;
   //echo $_SESSION["SelectedEMP"];
   mysqli_close($conn);
?>

