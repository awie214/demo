<!DOCTYPE html>
<html>
   <head>
      <?php
         $model = getvalue("hChildFile");
         include_once $files["inc"]["pageHEAD"];
      ?>
      <script src="<?php echo jsCtrl("ctrl_FileManager"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div id="mainScreen">
            <div class="panel-top">
               <a href="javascript:void(0)" class="mbar" id="titleBarIcons" onclick="openNav();">
                  <i class="fa fa-chevron-left" aria-hidden="true"></i>
               </a>&nbsp;
               <?php
                  if ($ScreenName == "") {
                     echo strtoupper(getvalue("paramTitle"));
                  } else {
                     echo strtoupper($ScreenName);
                  }
               ?>
               <button type="button" class="close" aria-label="Close" onclick="closeSCRN('model');">
                 <span aria-hidden="true" style="color:white;">&times;</span>
               </button>
            </div>
            <div class="container-fluid content">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="margin-top">
                        <div id="divList">
                           <span id="spGridTable">
                              <?php
                                 if ($model!="modelSysUser") $Action = [true,true,true,false];
                                                        else $Action = [false,false,false,false];
                                    $table = strtolower($table);
                                    if ($table == "schools") {
                                       $sql  = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 100";
                                    } else {
                                       $sql  = "SELECT * FROM `$table` ORDER BY RefId Desc";   
                                    }
                                    
                                    $_SESSION["module_sql"] = $sql;
                                    doGridTable($table,
                                                $gridTableHdr_arr,
                                                $gridTableFld_arr,
                                                $sql,
                                                $Action,
                                                $_SESSION["module_gridTable_ID"]);
                              ?>
                           </span>
                           <?php
                              if ($model!="modelSysUser") {
                                 btnINRECLO([true,true,true]);
                              }
                           ?>
                        </div>
                        <div id="divView">
                           <div class="mypanel panel panel-default">
                              <div class="panel-top">
                                 <span id="ScreenMode">INSERTING NEW</span> <?php echo strtoupper($ScreenName); ?>
                              </div>
                              <div class="panel-mid-litebg" id="EntryScrn">
                                 <?php
                                    require_once $model.".e2e.php";
                                 ?>
                              </div>
                              <div class="panel-bottom">
                                 <?php btnSACABA([true,true,true]); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
            <input type="hidden" name="hModel" value="<?php echo getvalue("hModel"); ?>">
            <?php
               footer();
               doHidden('isFileManager',"YES","");
               doHidden('hGridTable',$table,"");
               doHidden('hGridTableHdr',$gridTableHdr,"");
               doHidden('hGridTableFld',$gridTableFld,"");
               doHidden('hGridTableId',"gridTable","");
               doHidden('hGridTableAction','true,true,true',"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



