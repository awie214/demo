<?php
	include 'pms_conn_2.e2e.php';
	include 'conn.e2e.php';
	$emprefid = $_GET["hEmpRefId"];
	$hris_sql = "SELECT * FROM employees WHERE RefId = '$emprefid'";
	$hris_rs  = mysqli_query($conn,$hris_sql);
	function pms_FindFirst($table,$where) {
		include 'pms_conn_2.e2e.php';
		$sql = "SELECT * FROM $table $where";
		$rs  = mysqli_query($pms,$sql);
		if ($rs) {
			$row = mysqli_fetch_assoc($rs);
			return $row;
		} else {
			return false;
		}
	}
	function pms_SelectEach($table,$where) {
		include 'pms_conn_2.e2e.php';
		$sql = "SELECT * FROM $table $where";
		$rs  = mysqli_query($pms,$sql);
		if ($rs) {
			return $rs;
		} else {
			return false;
		}
	}
	function pms_GetName($table,$refid,$fld) {
		include 'pms_conn_2.e2e.php';
		$sql = "SELECT `$fld` FROM $table WHERE id = '$refid'";
		$rs  = mysqli_query($pms,$sql);
		if ($rs) {
			$row = mysqli_fetch_assoc($rs);
			return $row;
		} else {
			return false;
		}
	}

	function pms_GetLoan($emprefid,$name,$Ftable,$InfoTable,$FColumn,$rtnVal) {
		$refid = pms_FindFirst($Ftable,"WHERE name = '$name'");
		if ($refid) {
			$id = $refid["id"];
		} else {
			$id = 0;
		}
		if ($id > 0) {
			$pms_info = pms_FindFirst($InfoTable,"WHERE employee_id = '$emprefid' AND $FColumn = '$id'");
			if ($pms_info) {
				return $pms_info[$rtnVal];
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	$BASIC 					= 0;
	$PERA_id 				= 0;
	$PERA 					= 0;
	$Overtime 				= 0;
	$Uniform_Allowance 	= 0;
	$CNA_PEI 				= 0;
	$Opt_Ins 				= 0;
	$Genesis 				= 0;
	$Hosp_Plus 				= 0;
   $Division            = "";
   $PositionItem        = "";
   $Position            = "";
   $Office              = "";
	if ($hris_rs) {
		$hris_row 	= mysqli_fetch_assoc($hris_rs);
		$AgencyId 	= $hris_row["AgencyId"];
		$FirstName 	= $hris_row["FirstName"];
		$LastName 	= $hris_row["LastName"];
		$MiddleName = $hris_row["MiddleName"];
		$FullName   = $LastName.", ".$FirstName." ".$MiddleName;
		$pms_sql    = "SELECT * FROM pms_employees WHERE employee_number = '$AgencyId'";
		$pms_rs     = mysqli_query($pms,$pms_sql);
		if ($pms_rs) {
			$pms_row = mysqli_fetch_assoc($pms_rs);
			$pms_refid = $pms_row["id"];
			/*------------------------------------------------*/
			$pms_salary_info = pms_FindFirst("pms_salaryinfo","WHERE employee_id = '$pms_refid'");
			if ($pms_salary_info) {
				$BASIC = $pms_salary_info["salary_new_rate"];
			}
			/*------------------------------------------------*/
			$PERA_refid = pms_FindFirst("pms_benefits","WHERE name = 'PERA'");
			if ($PERA_refid) {
				$PERA_id = $PERA_refid["id"];
			}
			/*------------------------------------------------*/
			/*------------------------------------------------*/
			/*------------------------------------------------*/
			if ($PERA_id > 0) {
				$pms_benefits_info = pms_FindFirst("pms_benefitsinfo","WHERE employee_id = '$pms_refid' AND benefit_id = '$PERA_id'");
				if ($pms_benefits_info) {
					$PERA = $pms_benefits_info["benefit_amount"];
				}
			}
			/*------------------------------------------------*/
			/*------------------------------------------------*/
			$pms_payroll_info = pms_FindFirst("pms_payroll_information","WHERE employee_id = '$pms_refid'");
			if ($pms_payroll_info) {
				$gsis_contribution 			= $pms_payroll_info["gsis_contribution"];
				$philhealth_contribution 	= $pms_payroll_info["philhealth_contribution"];
				$pagibig_contribution 		= $pms_payroll_info["pagibig_contribution"];
				$tax_contribution 			= $pms_payroll_info["tax_contribution"];
			} else {
				$gsis_contribution 			= 0;
				$philhealth_contribution 	= 0;
				$pagibig_contribution 		= 0;
				$tax_contribution 			= 0;
			}

			$gsis_loan 		= pms_GetLoan($pms_refid,"CONSOLOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$pagibig_mpl 	= pms_GetLoan($pms_refid,"MULTI PURPOSE LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$policy 			= pms_GetLoan($pms_refid,"POLICY LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$WF_loan			= pms_GetLoan($pms_refid,"WELFARE FUND LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$WASSSLAI_loan	= pms_GetLoan($pms_refid,"WASSSLAI LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
         $EL_loan       = pms_GetLoan($pms_refid,"GSIS EMERGENCY LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
         $Optional_loan = pms_GetLoan($pms_refid,"OPTIONAL LIFE","pms_loans","pms_loansinfo","loan_id","loan_amortization");
         $Educ_loan     = pms_GetLoan($pms_refid,"EDUCATION ASST LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			//$MPLP_loan		= pms_GetLoan($pms_refid,"WASSSLAI LOAN","pms_loans","pms_loaninfo","loan_id","loan_amortization");
			$MPLP_loan     = 0;



			$WF_Contribution 	=  pms_GetLoan($pms_refid,"WELFARE FUND","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$WASSSLAI_Dep 		=  pms_GetLoan($pms_refid,"WASSSLAI Capital Contribution","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$Union_Dues 		=  pms_GetLoan($pms_refid,"UNION DUES","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$MP_Coop 			=  pms_GetLoan($pms_refid,"MULTI-PURPOSE COOPERATIVE CONTRIBUTION","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$Agency_Fee = 0;
		}
	}
	$Total_Earnings = $BASIC + $PERA + $Overtime + $CNA_PEI;
	$Total_Deductions = $gsis_contribution + $pagibig_contribution + $philhealth_contribution + $Opt_Ins + $Genesis + $Hosp_Plus +
								$gsis_loan + $pagibig_mpl + $policy + $WF_Contribution + $WF_loan + $WASSSLAI_Dep + $WASSSLAI_loan +
								$tax_contribution + $MPLP_loan + $Union_Dues + $MP_Coop + $Agency_Fee;
	$NET_Earnings = $Total_Earnings - $Total_Deductions;
?>
<!DOCTYPE>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
	<script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
      });
   </script>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar ("PAYROLL"); ?>
         <div class="container-fluid margin-top">
            <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
            <div class="row">
               <div class="col-xs-6" id="div_CONTENT">
                  <div class="container-fluid rptBody">
                     <div class="row">
                     	<div class="col-xs-12">
                     		<?php rptHeader("EMPLOYEE PAYSLIP"); ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <?php echo $Division; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                     	<div class="col-xs-12">
                     		<?php echo $PositionItem.", ".$FullName; ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           Employee No: <?php echo $AgencyId; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                     	<div class="col-xs-12">
                     		Monthly Salary: <?php echo $BASIC; ?>
                     	</div>
                     </div>
                     <div class="row margin-top" style="border: 1px solid black;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-4">
                                 <b>PAY PERIOD</b>
                              </div>
                              <div class="col-xs-4">
                                 <b>PARTICULARS</b>
                              </div>
                              <div class="col-xs-4">
                                 <b>AMOUNT</b>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-8">
                                 <b>Pay Period Salary</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <?php
                                    echo $BASIC;
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-8">
                                 <b>Total Amount of Deduction</b>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <?php
                                    echo $Total_Deductions;
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 Leave Without Pay (LWOP)
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 PHILHEALTH Contribution
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 HDMF Pag Ibig Contribution
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Contribution
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Optional Insurance Premium
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Optional Insurance Premium2
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Cash Advance Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Educhild
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Genesis Plus
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Educational Assistance Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Policy Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Emergency Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS UMID Cash Advance
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 GSIS Consoloan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 HDMF Housing Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 HDMF Multipurpose Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 HDMF Calamity Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 HDMF Housing Loan/LMLS Loan
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 NHMFC
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 PLDT/Cellphone
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-7">
                                 Withholding Tax
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-8">
                              </div>
                              <div class="col-xs-4" style="border-bottom: 1px solid black;">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-8">
                                 <b>Net Pay for the Period</b>
                              </div>
                              <div class="col-xs-4 text-right">

                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <qoute>
                     	This is a computer generated document and does not require any signature if without alterations
                     </qoute>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>