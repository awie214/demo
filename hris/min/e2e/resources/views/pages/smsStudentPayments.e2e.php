<?php
   $jsonFile = "StudentPayments";
   $moduleContent = file_get_contents(json.$jsonFile.'.json');
   $module        = json_decode($moduleContent, true); 
   $objs          = $module["Fields"];
   $label         = $module["Label"];
   $inputType     = $module["InputType"];
   $class         = $module["Class"];
   $defvalue      = $module["DefaultValue"];
   $css           = $module["css"];
   $count         = count($objs);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php 
         include_once $files["inc"]["pageHEAD"]; 
         include "inc/inc_External.e2e.php";
         if (getvalue("SaveSuccess") == "yes") {
            balloonMsg("Record ".getvalue("NewRecord")." Successfully Save","success");
         }
      ?>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php include "inc/inc_hdr.e2e.php"; ?>
         <div style="margin-top:60px;">
            <?php doSideBarMain(); ?>
            <div class="container-fluid" id="mainScreen">
               <?php
                  doTitleBar($module["Title"]);
                  spacer(5);
                  include "inc/inc_gridTable.e2e.php";
                  include "inc/inc_ModalDataEntry.e2e.php";
                  include "varHidden.e2e.php";
                  doHidden("fn","","");
                  doHidden("json",$jsonFile,"");
                  footer();
               ?>
            </div>
         </div> 
      </form>
   </body>
</html>



