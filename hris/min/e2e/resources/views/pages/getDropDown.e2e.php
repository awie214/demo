<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';	
	include pathClass.'0620functions.e2e.php';
	$objname 		= getvalue("obj");
	$objid 			= getvalue("obj");
	$table 			= getvalue("table");
	$batch 			= getvalue("batch");
	$srchString 	= getvalue("srchString");
	$sql 			= "SELECT *, (@rownum := @rownum + 1) AS rank FROM ".$table;
	if ($srchString != "") {
       	$sql = "SELECT *, (@rownum := @rownum + 1) AS rank FROM ".$table." WHERE Name LIKE '$srchString%' ORDER BY Name";
	}
	mysqli_query($conn,"set @rownum = 0");
	$result = mysqli_query($conn,$sql);
	$count = mysqli_num_rows($result);
		    echo '
			<div class="row" style="padding: 5px;">
				<div class="col-xs-4">
					<input type="text" class="form-input" placeholder="Search.." name="search" id="search" value="'.$srchString.'">
				</div>
				<div class="col-xs-2">
					<button type="button" 
							class="btn-cls2-red" 
							onclick="load_dropdown(\''.$table.'\',\''.$objname.'\',1,$(\'#search\').val());">
						Search
					</button>
				</div>
			</div>
			<div class="row" style=" max-height: 500px; overflow: auto; padding: 10px;">
				<table class="table" border=1>
					<thead>
						<tr>
							<th class="text-center" style="width:100px">ACTION</th>
							<th class="text-center" style="width:100px">REFID</th>
							<th class="text-center" style="width:*px">NAME</th>
						</tr>
					</thead>
					<tbody>
			';
			
		        $seekRow_Start = (($batch - 1) * 20) + 1;
                $seekRow_End = ($seekRow_Start + 20) - 1;
                $j = 0;
                while ($row = mysqli_fetch_assoc($result)) {

                	if ($row["rank"] >= $seekRow_Start && 
                		$row["rank"] <= $seekRow_End) {

                		$name = mysqli_real_escape_string($conn,$row["Name"]);
                	    echo '	
						<tr>
							<td class="text-center">
								<button type="button" 
										class="btn-cls2-sea" 
										onclick="insert_dropdown('.$row["RefId"].',\''.$objname.'\',\''.$name.'\');">
							  		SELECT ME
						  		</button>
							</td>
							<td class="text-center">'.$row["RefId"].'</td>
							<td>'.$row["Name"].'</td>
						</tr>';	
                	}   
					
				}
			echo '
					</tbody>
				</table>
			</div>
			<div class="row margin-top text-right" style="padding: 5px;">
				<div class="col-md-4">
					<select class="form-input" name="drp_'.$table.'" onchange="load_dropdown(\''.$table.'\',\''.$objname.'\',$(this).val(),\'\');">
			';
				if ($count < 20) {
					echo '<option value="1">Page 1</option>';
				} else {
					for($i=1;$i<=($count / 20);$i++) {
						echo '<option value="'.$i.'">Page '.$i.'</option>';
					}	
				}
				
			echo '
					</select>
				</div>
			</div>
			<script>
				$("[name=\'drp_'.$table.'\']").val('.$batch.');
			</script>
			';	
?>