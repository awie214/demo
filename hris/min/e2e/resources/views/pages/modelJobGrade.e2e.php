<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container-fluid" id="EntryScrn">
      <div class="row">
         <div class="col-sm-12">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
               <div class="row">
                  <div class="col-sm-4">
                     <label>NAME:</label><br>
                     <input type="text" class="form-input saveFields-- mandatory" name="char_Name" autofocus>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-sm-2">
                     <div class="border padd5"> 
                        <label>Step 1:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step1">
                        <label>Annual (Step 1):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS1">
                     </div>   
                  </div>
                  <div class="col-sm-2">
                     <div class="border padd5"> 
                        <label>Step 2:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step2">
                        <label>Annual (Step 2):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS2">
                     </div>   
                  </div>
                  <div class="col-sm-2">
                     <div class="border padd5">
                        <label>Step 3:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step3">
                        <label>Annual (Step 3):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS3">
                     </div>   
                  </div>
                  <div class="col-sm-2">
                     <div class="border padd5"> 
                        <label>Step 4:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step4">
                        <label>Annual (Step 4):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS4">
                     </div>   
                  </div>
               </div>   
               <div class="row margin-top">
                  <div class="col-sm-2">
                     <div class="border padd5">
                        <label>Step 5:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step5">
                        <label>Annual (Step 5):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS5">
                     </div>   
                  </div>
                  <div class="col-sm-2">
                     <div class="border padd5">
                        <label>Step 6:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step6">
                        <label>Annual (Step 6):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS6">
                     </div>   
                  </div>
                  <div class="col-sm-2">
                     <div class="border padd5">
                        <label>Step 7:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step7">
                        <label>Annual (Step 7):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS7">
                     </div>   
                  </div>
                  <div class="col-sm-2">
                     <div class="border padd5">
                        <label>Step 8:</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_Step8">
                        <label>Annual (Step 8):</label><br>
                        <input type="text" class="form-input saveFields-- number-- mandatory" name="deci_AnnualS8">
                     </div>   
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 form-group">
                     <label class="control-label" for="inputs">REMARKS:</label>
                     <textarea class="form-input saveFields--" rows="5" name="char_Remarks"
                     placeholder="remarks"><?php echo $remarks; ?></textarea>
                  </div>
               </div>
         </div>
      </div>
   </div>