<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language = "JavaScript">
         $(document).ready(function () {
            $("#cancelUsername, #changeUsername, #saveUsername").prop("disabled",true);
            $("[name='hToken']").append("<input type='hidden' id='dummy' name='dummy'>");
            $("#changeUsername").click(function () {
               $(this).prop("disabled",true);
               $("#UName").removeClass("form-disp");
               $("#UName").addClass("form-input");
               $("#cancelUsername, #saveUsername").prop("disabled",false);
            });
            $("#cancelUsername").click(function () {
               $("#UName").removeClass("form-input");
               $("#UName").addClass("form-disp");
               $(this).prop("disabled",true);
               $("#saveUsername").prop("disabled",true);
               $("#changeUsername").prop("disabled",false);
               var emprefid = $("[name='hRefIdSelected']").val();
               retrieveUsername(emprefid);
            });
            $("#saveUsername").click(function () {
               var value = $("#UName").val();
               var emprefid = $("[name='hRefIdSelected']").val();
               updateUsername(value,emprefid);
            });
         });
         function updateUsername(value,emprefid) {
            $.post("trn.e2e.php",
            {
               fn:"updateUsername",
               value:value,
               emprefid:emprefid
            },
            function(data,status){
               if (status == 'success') {
                  data = data.trim();
                  if (data > 0) {
                     $.notify("Username Successfully Updated","success");
                     $("#cancelUsername, #saveUsername").prop("disabled",true);
                     $("#changeUsername").prop("disabled",false);
                     $("#UName").removeClass("form-input");
                     $("#UName").addClass("form-disp");
                  } else {
                     $.notify("Error in Updating Username");
                  }
               }
            });
         }
         function retrieveUsername(emprefid) {
            $.post("trn.e2e.php",
            {
               fn:"retrieveUsername",
               emprefid:emprefid
            },
            function(data,status){
               if (status == 'success') {
                  data = data.trim();
                  eval(data);
               }
            });
         }
         function selectMe(emprefid) {
            $.get("EmpQuery.e2e.php",
            {
               emprefid:emprefid
            },
            function(data,status) {
               if(status == "error") return false;
               if(status == "success") {
                  var data = JSON.parse(data);

                  try {
                     setValueByName("hRefIdSelected",emprefid);
                     <?php echo "var path = '".path."';" ?>   
                     $('#empRefId').html(data.RefId + "&nbsp;");
                     $('#lastName').html(data.LastName + "&nbsp;");
                     $('#firstName').html(data.FirstName + "&nbsp;");
                     $('#middleName').html(data.MiddleName + "&nbsp;");
                     $('#extName').html(data.ExtName + "&nbsp;");
                     $('#nickName').html(data.NickName + "&nbsp;");
                     $('#contactNo').html(data.ContactNo + "&nbsp;");
                     $('#emailAdd').html(data.EmailAdd + "&nbsp;");
                     $('#AgencyId').html(data.AgencyId + "&nbsp;");
                     $('#dummy').val(data.UserName);
                     $("#tSecret").val("");
                     if (data.BirthDate == '1900-01-01')
                        $('#birthDate').html("");
                     else
                        $('#birthDate').html(data.BirthDate + "&nbsp;");
                     $('#UName').val(data.UserName);
                     $('#EmployeesPic').attr("src",path + "images/" + data.CompanyRefId + "/EmployeesPhoto/" + data.PicFilename);
                     $("#changeUsername").prop("disabled",false);
                  }
                  catch (e)
                  {
                     if (e instanceof SyntaxError) {
                        alert(e.message);
                     }
                  }
               }
            });
         }
         <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && 
                getvalue("hRefIdSelected") > 0) {
               $pw = getvalue("hToken");   
               $id = getvalue("hRefIdSelected");   
               $sql = "UPDATE `employees` SET pw = '$pw' WHERE RefId = $id";
               if (!($conn->query($sql) === TRUE)) {
               echo "alert('Error Password Generation !!!');";
               } else {
                  $fields = "`EmployeesRefId`,`TabsName`,`FieldsEdit`,`TableName`,`OldValue`, `NewValue`,`Status`,`TrnDate`,`TrnTime`, ";
                  $values = "'$id','Reset PW','pw','employees','Reset', 'Default','A','".date("Y-m-d",time())."','".date("H:i:s",time())."', ";
                  $lastid = f_SaveRecord("NEWSAVE","Updates201",$fields,$values);
                  echo "setValueByName('hRefIdSelected',0);";
                  echo "$.notify('Reset Password Successfully Completed!!!','success');";
                  
               } 
            }
         ?>
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Employees Reset Password");?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row" id="divList">
                        <div class="col-sm-4">
                           <?php employeeSelector(); ?>
                        </div>
                        <div class="col-sm-8">
                           <div class="mypanel">
                              <div class="panel-top"></div>
                              <div class="panel-mid">
                                 <div class="row">
                                    <div class="col-xs-3">
                                       
                                       <img id="EmployeesPic" src=""
                                          style="margin-bottom:3px;border:1px solid #aaa;width:1.2in;height:1.5in;"/><br>
                                    </div>
                                    <div class="col-xs-8">
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label>Reference Id:</label>
                                          </div>
                                          <div class="col-xs-2">
                                             <div class="form-disp" id="empRefId">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label>Employee Id:</label>
                                          </div>
                                          <div class="col-xs-4">
                                             <div class="form-disp" id="AgencyId">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label>Employees Username:</label>
                                          </div>
                                          <div class="col-xs-4">
                                             <input type="text" name="UName" id="UName" class="form-disp">
                                          </div>
                                          <div class="col-xs-4">
                                             <button type="button" id="changeUsername" class="btn-cls4-tree">
                                                <i class="fa fa-edit"></i>
                                             </button>
                                             <button type="button" id="saveUsername" class="btn-cls4-sea">
                                                <i class="fa fa-save"></i>
                                             </button>
                                             <button type="button" id="cancelUsername" class="btn-cls4-red">
                                                <i class="fa fa-close"></i>
                                             </button>

                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label>Last Name :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="lastName">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label for="firstName">First Name :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="firstName">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label for="middleName">Middle Name :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="middleName">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label for="NickName">Ext. Name :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="extName">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label for="NickName">Nick Name :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="nickName">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label for="birthDate">Date of Birth :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="birthDate">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label for="NickName">Contact No. :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="contactNo">&nbsp;</div>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 label">
                                             <label for="NickName">Email Address :</label>
                                          </div>
                                          <div class="col-xs-8">
                                             <div class="form-disp" id="emailAdd">&nbsp;</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <br><br>
                                 <div class="row margin-top">
                                    <div class="col-xs-6 label">
                                       <label><span><b>**</b></span>&nbsp;PASSWORD :</label>
                                    </div>
                                    <div class="col-xs-6">
                                       <input type="password" name="tSecret"
                                          maxlength=15 id="tSecret"
                                          placeholder="HR Admin Password"
                                          class="form-input" value="" style="width:150px;">
                                    </div>
                                 </div>
                                 <br><br>
                                 <div class="row margin-top txt-center">
                                    <button type="button" class="btn-cls4-red"
                                             name="btnRESETPW" id="btnRESETPW" value="Submit">
                                       EMPLOYEES PASSWORD RESET
                                    </button>
                                 </div>
                                 <br>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>   
                        </div>
                     </div>   
               </div>
            </div>
            <input type="hidden" name="hToken" value="">
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



