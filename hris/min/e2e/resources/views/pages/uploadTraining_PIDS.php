<!DOCTYPE html>
<html>
<body>

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";

            $conn->query("TRUNCATE TABLE `employeestraining`;");

            $t = time();
            $date_today    = date("Y-m-d",$t);
            $curr_time     = date("H:i:s",$t);
            $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
            $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

            $sql = "SELECT * FROM `employee_training_programs`";
            $rs = mysqli_query($pids_conn,$sql) or die(mysqli_error($pids_conn));
            echo "Number Records : ".mysqli_num_rows($rs)."<br>";
            if ($rs) {
               if (mysqli_num_rows($rs) > 0) {
                  while ($pids = mysqli_fetch_array($rs)) {
                     $refid = $pids["employee_personal_information_sheet_id"];
                     $empRefId = $refid;

                     $wSeminarRefId = 0;
                     $wSeminar = remquote($pids['title_of_seminar']);
                     if ($wSeminar != ""){
                        $flds = "`Code`,`Name`,`Remarks`,".$trackingA_fld;
                        $values = "'','$wSeminar','Migrated',".$trackingA_val;
                        $isExist = FindFirst("seminars","where Name = '$wSeminar'","RefId");
                        if (!$isExist) {
                           $sql = "INSERT INTO `seminars` ($flds) VALUES ($values)";
                           if ($conn->query($sql) === TRUE) {
                              $wSeminarRefId = mysqli_insert_id($conn);

                           } else {
                              echo $pids['title_of_seminar']." Is Not Inserted!";
                           }
                        } else {
                           $wSeminarRefId = $isExist;
                        }


                     }

                     $wSponsorRefId = 0;
                     $wSponsor = remquote($pids['conducted_by']);
                     if ($wSponsor != ""){
                        $flds = "`Code`,`Name`,`Remarks`,".$trackingA_fld;
                        $values = "'','$wSeminar','Migrated',".$trackingA_val;
                        $isExist = FindFirst("sponsor","where Name = '$wSponsor'","RefId");
                        if (!$isExist) {
                           $sql = "INSERT INTO `sponsor` ($flds) VALUES ($values)";
                           if ($conn->query($sql) === TRUE) {
                              $wSponsorRefId = mysqli_insert_id($conn);

                           } else {
                              echo $pids['conducted_by']." Is Not Inserted!";
                           }
                        } else {
                           $wSponsorRefId = $isExist;
                        }


                     }

                     $wStartDate = $pids['start_date'];
                     $wEndDate = $pids['end_date'];
                     $wNumofHrs = $pids['hours'];

                     $flds = "";
                     $values = "";
                     $flds .= "`CompanyRefId`,`BranchRefId`,`EmployeesRefId`, `SeminarsRefId`, `StartDate`, `EndDate`, `NumofHrs`,`SponsorRefId`,".$trackingA_fld;
                     $values .= "1000,1,$empRefId, $wSeminarRefId, '$wStartDate', '$wEndDate', '$wNumofHrs','$wSponsorRefId',".$trackingA_val;


                     $sql = "INSERT INTO `employeestraining` ($flds) VALUES ($values)";
                     if ($conn->query($sql) === TRUE) {
                        echo "Migrated Training -->$empRefId<br>";
                     }
                     else {
                        echo "ERROR Migration Training -->$empRefId<br>";
                     }
                  }
               }
            }
            mysqli_close($conn);


?>

</body>
</html>