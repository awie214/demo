<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("SIGNATORIES"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row">
                        <div class="col-xs-4">
                           <span class=""><h4>Signatory Information</h4></span>
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Label 1</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Label1"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Name 1</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Name1"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Position 1</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Position1"/>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6 txt-right">
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Label 2</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Label2"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Name 2</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Name2"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Position 2</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Position2"/>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Label 3</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Label3"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Name 3</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Name3"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Position 3</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Position3"/>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6 txt-right">
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Label 4</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Label4"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Name 4</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Name4"/>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3 txt-right">
                                 <span class="label">Position 4</span>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text" class="form-input" name="char_Position4"/>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>




