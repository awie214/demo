<?php
   include_once 'constant.e2e.php';
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'PMSFunctions.e2e.php';
	include_once 'conn.e2e.php';
	include_once 'inc/inc_pms_payslip.e2e.php';
?>
<!DOCTYPE>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
	<script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
      });
   </script>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar ("PAYROLL"); ?>
         <div class="container-fluid margin-top">
            <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <div class="container-fluid rptBody">
                     <div class="row">
                     	<div class="col-xs-12">
                     		<?php rptHeader("EMPLOYEE PAYSLIP"); ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           Employee Name: <?php echo $FullName; ?>
                        </div>
                        <div class="col-xs-6">
                           Emp No: <?php echo $AgencyId; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           Position: <?php echo $Position; ?>
                        </div>
                        <div class="col-xs-6">
                           Division: <?php echo $Division; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <div class="row" style="border:  1px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>BASIC SALARY</b>
                              </div>
                           </div>
                           <div class="row margin-top" style="padding: 10px;">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       BASIC:
                                    </div>
                                    <div class="col-xs-6 text-right">
                                       <?php echo number_format($BASIC,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       PERA:
                                    </div>
                                    <div class="col-xs-6 text-right">
                                       <?php echo number_format($PERA,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       RATA:
                                    </div>
                                    <div class="col-xs-6 text-right">
                                       <?php echo number_format($RATA,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row" style="border:  1px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>BENEFITS</b>
                              </div>
                           </div>
                           <div class="row margin-top" style="padding: 10px;">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       SALA:
                                    </div>
                                    <div class="col-xs-6 text-right">
                                       <?php echo number_format($SALA,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       Hazard Pay:
                                    </div>
                                    <div class="col-xs-6 text-right">
                                       <?php echo number_format($Hazard_Pay,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       Longevity Pay:
                                    </div>
                                    <div class="col-xs-6 text-right">
                                       <?php echo number_format($Longevity_Pay,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="row" style="border:  1px solid black;">
                              <div class="col-xs-12 text-center">
                                 <b>DEDUCTIONS</b>
                              </div>
                           </div>
                           <div class="row margin-top" style="padding: 10px;">
                              <div class="col-xs-12">
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       GSIS:
                                    </div>
                                    <div class="col-xs-3 text-right">
                                       <?php echo number_format($gsis_contribution,2); ?>
                                    </div>
                                    <div class="col-xs-3">
                                       Withholding Tax:
                                    </div>
                                    <div class="col-xs-3 text-right">
                                       <?php echo number_format($tax_contribution,2); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3">
                                       Pagibig:
                                    </div>
                                    <div class="col-xs-3 text-right">
                                       <?php echo number_format($pagibig_contribution,2); ?>
                                    </div>
                                    <div class="col-xs-3">
                                       Philhealth:
                                    </div>
                                    <div class="col-xs-3 text-right">
                                       <?php echo number_format($philhealth_contribution,2); ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <br>
                     <br>
                     <br>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-6 text-right">
                           <b>GROSS INCOME: &nbsp;&nbsp;&nbsp;&nbsp;</b>
                           <?php
                              $Gross_Income = $BASIC + $PERA + $RATA + $SALA + $Hazard_Pay + $Longevity_Pay;
                              echo number_format($Gross_Income,2);
                           ?>
                        </div>
                        <div class="col-xs-6 text-right">
                           <b>TOTAL DEDUCTIONS: &nbsp;&nbsp;&nbsp;&nbsp;</b>
                           <?php
                              $Total_Deductions = $gsis_contribution + $pagibig_contribution + $philhealth_contribution + $tax_contribution; 
                              echo number_format($Total_Deductions,2);
                           ?>
                        </div>
                     </div>
                     <br>
                     <br>

                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <b>NET INCOME <?php echo date("M Y",time()); ?></b>
                        </div>
                        <div class="col-xs-3 text-right">
                           <?php echo number_format(($Gross_Income - $Total_Deductions),2) ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">
                           <b>NET HALF</b>
                        </div>
                        <div class="col-xs-3 text-right">
                           <?php echo number_format((($Gross_Income - $Total_Deductions) / 2),2) ?>
                        </div>
                        <div class="col-xs-6 text-center">
                           __________________________________________
                           <br>
                           SIGNATURE
                        </div>
                     </div>
                     <br>
                     <br>
                     <qoute>
                     	This is a computer generated document and does not require any signature if without alterations
                     </qoute>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>