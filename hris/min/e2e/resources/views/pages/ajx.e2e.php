<?php
   session_start();
   include_once "conn.e2e.php";
   include_once "colors.e2e.php";
   include_once "constant.e2e.php";
	include_once pathClass.'0620functions.e2e.php';
   
   $task = getvalue("htask");
   
   if ($task == "init") {
   }
   else if ($task == "loadscrn") {
      
      $p = fopen('syslog/'.$_SESSION['sess_user'].'_ModulesUsed_'.$today.'.log', 'a+'); 
      $_SESSION['hGRPERIOD'] = 0;
      $_SESSION['hSUBJECTS'] = 0;
      $SubMenuID = getvalue("mid");
      $_SESSION['hSUBMENUID'] = $SubMenuID;
      $sql = "WHERE SchoolId = ".$_SESSION['SchoolId'];
      $sql .= " AND MenuCode = $SubMenuID";
      $row = FindFirst("SysModule",$sql,"*");
      $url = $row["ProgFile"]."?";
      $_SESSION['table']     = $row["TableName"];
      $_SESSION['colhdr']    = explode("|",$row["ColHdr"]); 
      $_SESSION['fieldname'] = explode("|",$row["FldName"]); 
      $_SESSION['align']     = explode("|",$row["DataAlign"]); 
      $_SESSION['RefKey']    = $row["RefKey"];
      $url .= $_SESSION['sess_gParam']."&mid=".$SubMenuID;
      fwrite($p,$datelog." URL:".$url."\n");
      echo "self.location = '$url';";
      fclose($p);
   }
   else if ($task == "SaveDataEdit") {
      $fieldnvalue = "";
      $val   = explode("|",getvalue("val"));
      $fname = explode("|",getvalue("fname"));
      for ($j=0;$j<(sizeof($fname) - 1);$j++) {
         $fieldnvalue = $fieldnvalue . $fname[$j] . "='" . $val[$j] . "',";
      }
      $fieldnvalue = $fieldnvalue.$trackingE;
      $qry_update = "UPDATE ".getvalue("tbl")." SET ".$fieldnvalue." WHERE RefId = ".getvalue("id");
      if (mysqli_query($conn, $qry_update)) {
         echo "alert('Record Successfully Save');";
         echo "location.reload();";
      } 
      else echo "alert('Error updating record >> ".mysqli_error($conn)."');";
   }
   else if ($task == "updStudentActivity") {
            /*url += "&hRefId=" + RefId;
            url += "&StudentRefId=" + d.txtEntry_fin_StudentRefId.value;
            url += "&EnrollId=" + EnrollId; //d.drpEnrollment.value;
            url += "&ActivityRefId=" + d.drpActivityRefId.value;
            url += "&Score=" + d.txtScore.value;
            url += "&Grade=" + d.txtGrades.value;
            url += "&TotItem=" + d.txtTotalItem.value;
            url += "&hTABLE=StudentActivity";*/
      $fieldnvalue  = "ActivityRefId = ".getvalue("ActivityRefId");
      $fieldnvalue .= ",Score = ".getvalue("Score");
      $fieldnvalue .= ",TotalItem = ".getvalue("TotItem");
      $fieldnvalue .= ",Grades = ".getvalue("Grade");
      $fieldnvalue .= ",".$trackingE;
      $qry_update = "UPDATE StudentActivity SET ".$fieldnvalue." WHERE RefId = ".getvalue("hRefId");
      if (!(mysqli_query($conn, $qry_update))) {
         echo "alert('Error updating record >> ".mysqli_error($conn)."');";
      }   
      echo "closePop();";
      echo "setScreen(".getvalue("hRefId").");";
   }
   
   else if ($task == "loadBrowser") { 
      $StudentRefId = getvalue("StudentRefId");
      $RefId = getvalue("RefId");
      $_SESSION['hGRPERIOD'] = getvalue("hPeriod");
      $_SESSION['hSUBJECTS'] = getvalue("hSubjectId");
      $table = getvalue("hTABLE");
      if ($table=="") $table = $_SESSION['table'];
      echo "document.getElementById('tdLIST').innerHTML = '". doBrowser($table,
                                                                        $_SESSION['colhdr'],
                                                                        $_SESSION['fieldname'],
                                                                        $_SESSION['align'],
                                                                        $StudentRefId,
                                                                        $RefId)."';";
      
   }
   else if ($task == "Load_EditScreen_3504") { 
      $StudentRefId     = getvalue("StudentRefId");
      $RefId            = getvalue("hRefId");
      $ActivityRefId   = getvalue("ActivityRefId");
      $Period           = getvalue("hPeriod");
      $EnrollId         = getvalue("EnrollId"); 
      $table            = getvalue("hTABLE");
      echo "setBHV('styleDisplay','block','idBlack');";   
      echo "setBHV('styleDisplay','block','idPopOut');";   
      echo "setBHV('innerHTML','".EditScreen_3504($RefId,$ActivityRefId,$StudentRefId,$EnrollId)."','idPopOut');";   
      echo "var d = document.xForm;";
      /*echo "d.txtScore.onmouseover = function() { doTooltip(event,0,'Score of the Student',''); }";
      echo "d.txtScore.onmouseout = function() { hideTip(); }";
      echo "d.txtTotalItem.onmouseover = function() { doTooltip(event,0,'Total Items of the Exam',''); }";
      echo "d.txtTotalItem.onmouseout = function() { hideTip(); }";*/
      //echo "d.txtGrades.onmouseover = function() { alert(34); }";
      //echo "d.txtGrades.addEventListener('mouseover', doTooltip(event,0,'Grades of the Student',''));";
      //echo "d.txtGrades.addEventListener('mouseout', hideTip());";
   }
   
   else if ($task == "PopContent") { 
      $table     = getvalue("t");
      $file      = getvalue("f");
      $StudentSearch = getvalue("hStudent");
      $browse    = getvalue("brw");
      $fld_idx   = getvalue("idx");
      $objname   = getvalue("objfocus");
      $GLOBALS['search_LASTNAME'] = getvalue("ln");
      $GLOBALS['search_FIRSTNAME'] = getvalue("fn");
      $GLOBALS['search_STUDENTREFID'] = getvalue("sid");
      $GLOBALS['search_GRDLVL'] = getvalue("glvl"); 
      //echo showRecordList("students",$_SESSION['SchoolId'],$_SESSION['CampusId']);
      $htm = showRecordList("students",$_SESSION['SchoolId'],$_SESSION['CampusId']);
      echo "setBHV('innerHTML','".$htm."','tableBrowser');";  
      if ($objname == "") {
         echo 'var obj = document.xForm["txtLN"];';
      }
      else {
         if ($objname == "drpGLVL")
            echo 'var obj = document.xForm["txtLN"];';
         else 
            echo 'var obj = document.xForm["'.$objname.'"];';
      }
      echo 'SetCursorToEnd(obj);';
   }
   else if ($task == "LoadStudent") { 
      $LastName  = getvalue("ln");
      $gLVL      = getvalue("glvl");
      $sort      = getvalue("hSort");
      $Remove    = getvalue("hRemove");
      $ActivityRefId = getvalue("hActivityRefId");
      
      $htm  = '<table border="1" style="width:99%;">';
      $htm .= '<tr>';
      $htm .= '<th style="width:60px;" title="Student Ref. Id">SREFID</th>';
      $htm .= '<th style="width:*px;">LAST NAME</th>';
      $htm .= '<th style="width:*px;">FIRST NAME</th>';
      $htm .= '<th style="width:*px;">MIDDLE NAME</th>';
      $htm .= '<th style="width:60px;">GENDER</th>';
      $htm .= '<th style="width:60px;">GR. LVL</th>';
      $htm .= '<th style="width:40px;">&nbsp;</th>';
      $htm .= '<th style="width:60px;">&nbsp;</th>';
      $htm .= '</tr>';
      
      $sql = "select * from Students where Students.SchoolId = $SchoolId";
      if ($LastName != "") $sql .= " AND Students.LastName LIKE '$LastName%'";
      if ($gLVL != "")     $sql .= " and GradeLevel = $gLVL";
      if ($sort == "") $sort = "LastName";
      $sql .= " ORDER BY Students.$sort";
      $rsStudents = mysqli_query($conn,$sql) or die(mysqli_error());
      while ($row = mysqli_fetch_assoc($rsStudents)) {
         
         $month = date("m");
         if ($month >= 6 && $month <= 12) $SchYR = date("Y");
                                     else $SchYR = date("Y") - 1;
         
         $sql = "select * from StudentEnrollments where SchoolId = ".$row["SchoolId"];
         $sql .= " and SchoolYear = $SchYR";
         $sql .= " and StudentRefId = ".$row["RefId"];
         $sql .= " limit 1";
         $rsEnroll = mysqli_query($conn,$sql) or die(mysqli_error());
         while ($rowEnroll = mysqli_fetch_assoc($rsEnroll)) {
            $refid = str_pad($row["RefId"],4,"0",STR_PAD_LEFT);
            $xrefid = $row["RefId"];
            $StudInfo = "[".$refid."] ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"]." - ".$row["Gender"]." - ".$row["GradeLevel"];    
            $htm .= '<input type="hidden" value="'.$StudInfo.'" name="h_'.$xrefid.'">';
            $whereClause = "";
            $whereClause .= "WHERE SchoolId = $SchoolId";
            $whereClause .= " AND StudentRefId = $xrefid";
            if ($ActivityRefId) $whereClause .= " AND ActivityRefId = $ActivityRefId";
            $rs_StudentActivity = SelectEach("StudentActivity",$whereClause);
            $numResult = mysql_num_rows($rs_StudentActivity);
            
            if ($Remove == 'false') {
               if ($numResult) { // may record na
                  $htm .= '<tr style="background:transparent;">';
               }
               else { // wala pang record
                  $htm .= '<tr class="clsNoRecord">';
               }
                  $htm .= '<td style="text-align:center;">'.$refid.'</td>';
                  $htm .= '<td>'.$row["LastName"].'</td>';
                  $htm .= '<td>'.$row["FirstName"].'</td>';
                  $htm .= '<td>'.$row["MiddleName"].'</td>';
                  $htm .= '<td style="text-align:center;">'.$row["Gender"].'</td>';
                  $htm .= '<td style="text-align:center;">'.$row["GradeLevel"].'</td>';
                  
                  if ($numResult) { // may record na
                     $htm .= '<td colspan="2">&nbsp;</td>';
                  }
                  else {
                     $htm .= '<td style="text-align:center;"><input type="checkbox" id="chkSelected" class="clsSELECTED" onclick="txtfocus('.$xrefid.');"  name="chk_'.$xrefid.'"></td>';
                     $htm .= '<td style="text-align:center;padding:1px;"><input type="text" name="txt_'.$xrefid.'" style="font-size:10px;height:15px;width:60%;font-weight:600;"></td>';
                  }
                  
                  $htm .= '</tr>';
            }
            else {
               if (!($numResult)) { // wala pang record
                  $htm .= '<tr class="clsNoRecord">';
                  $htm .= '<td style="text-align:center;">'.$refid.'</td>';
                  $htm .= '<td>'.$row["LastName"].'</td>';
                  $htm .= '<td>'.$row["FirstName"].'</td>';
                  $htm .= '<td>'.$row["MiddleName"].'</td>';
                  $htm .= '<td style="text-align:center;">'.$row["Gender"].'</td>';
                  $htm .= '<td style="text-align:center;">'.$row["GradeLevel"].'</td>';
                  $htm .= '<td style="text-align:center;"><input type="checkbox" id="chkSelected" class="clsSELECTED" onclick="txtfocus('.$xrefid.');" name="chk_'.$xrefid.'"></td>';
                  $htm .= '<td style="text-align:center;padding:1px;"><input type="text" name="txt_'.$xrefid.'" style="font-size:10px;height:15px;width:60%;font-weight:600;"></td>';
                  $htm .= '</tr>';
               }
            }
            /*
            if (j<=0) {
               oOption.style.color="#f00";  
               oOption.style.fontWeight="600";  
            }
            */
         }
      }  
      $htm .= "</table>";
      echo "setBHV('innerHTML','".$htm."','dvStudSelector');";   
   }
   else if ($task=="Populate_drpEnrollment") {
      $StudentRefId = getvalue("StudentRefId");
      $obj = "drpEntry_fin_EnrollmentRefId";
      $sql  = "SELECT * FROM StudentEnrollments WHERE SchoolId = $SchoolId";
      $sql .= " AND StudentRefId = $StudentRefId ORDER BY RefId DESC";
      $result = $conn->query($sql);
      $myCounter = -1;
      $js  = 'var d = document.xForm;';
      $js .= "\n".'d.'.$obj.'.length = 0;';
      if ($result) {
         while($row = $result->fetch_assoc()) {
            $myCounter++;
            $objText = "[".$row["RefId"]."] - Enrll Date : ".$row["EnrollmentDate"]." - GradeLvl : ".$row["GradeLevel"]." - SY".$row["SchoolYear"]." Bal ".$row["Balance"];
            $js .= "\n".'d.'.$obj.'.options['.$myCounter.'] = new Option();';
            $js .= "\n".'d.'.$obj.'.options['.$myCounter.'].value = "'.$row["RefId"].'";';
            $js .= "\n".'d.'.$obj.'.options['.$myCounter.'].text = "'.$objText.'";';
         }
      }
      else {
         $js .= "\n".'d.'.$obj.'.options[0] = new Option();';
         $js .= "\n".'d.'.$obj.'.options[0].value = "0";';
         $js .= "\n".'d.'.$obj.'.options[0].text = "NO ENROLLMENT RECORD";';
      }
      $js .= "\n".'d.'.$obj.'.disabled = false';
      echo $js;
   }
   else if ($task=="FilterActivity") {
      $ObjName = getvalue("objname");
      $KindOfActivityRefId = getvalue("get");
      echo 'var d = document.xForm;
         d.'.$ObjName.'.length=0;
         d.'.$ObjName.'.options[0]=new Option();
         d.'.$ObjName.'.options[0].value="";
         d.'.$ObjName.'.options[0].text="";';
      $myCounter = 0;
      $where = "WHERE SchoolId = $SchoolId";
      $where .= " AND Username = '$user'";
      if ($KindOfActivityRefId!=="") $where .= " AND KindOfActivityRefId = $KindOfActivityRefId";
      $rs_Activity = SelectEach("Activity",$where);
      $numResult = mysql_num_rows($rs_Activity);
      if ($numResult) {
         while($row_Activity = mysql_fetch_assoc($rs_Activity)) {
            $actName  = FindFirst("KindOfActivity","WHERE Refid = ".$row_Activity["KindOfActivityRefId"],"ActivityName");
            $subjName = FindFirst("Subjects","WHERE Refid = ".$row_Activity["SubjectId"],"SubjectName");
            switch ($row_Activity["Period"]) {
               case 1: 
                  $period = "1st Period";
               break;
               case 2: 
                  $period = "2nd Period";
               break;   
               case 3: 
                  $period = "3rd Period";
               break;   
               case 4: 
                  $period = "4th Period";
               break;   
            }
            $optTEXT = $row_Activity["RefId"]." - SY".$row_Activity["SchoolYear"]." - ".$period." - ".$row_Activity["ActivityDate"]." - ".
                       $actName." - ".$subjName." - ".$row_Activity["Description"];
            $myCounter++;
            echo 
            'd.'.$ObjName.'.options['.$myCounter.']=new Option();
             d.'.$ObjName.'.options['.$myCounter.'].value="'.$row_Activity["RefId"].'";
             d.'.$ObjName.'.options['.$myCounter.'].text="'.$optTEXT.'";';
         }
      }
      else {
         echo 'alert("NO AVAILABLE ACTIVITY");';
      }
   }
   else if ($task == "logout") { 
      echo "self.location = 'SCRNwelcome.e2e.php?';";
   }
   else echo "alert('$task NO ASSIGNED TASK');";
?>