<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <!--<script language="JavaScript" src="<?php echo jsCtrl("ctrl_IndividualDevtPlan"); ?>"></script>-->
      <script language="JavaScript">
         $(document).ready(function () {
            $("[name*='sint_P5']").click(function () {
               if ($("[name*='sint_P5']:checkbox:checked").length == 0) {
                  $("#p5_exp").prop("disabled",true);
               } else {
                  $("#p5_exp").prop("disabled",false);
               }
            });
            $("#btnsaveDevPlan").click(function () {
               $.ajax({
                  url: "trn.e2e.php",
                  type: "POST",
                  data: new FormData($("[name='xForm']")[0]),
                  success : function(responseTxt){
                     //alert(responseTxt);
                     eval(responseTxt);
                  },
                  error: function(){
                     alert("error");
                     return false;
                  },
                  enctype: 'multipart/form-data',
                  processData: false,
                  contentType: false,
                  cache: false
               });
            });
         });
         function selectMe(emprefid) {
            $("[name=\'hEmpRefId\']").val(emprefid);
            $.get("ldmsEmpDetail.e2e.php",
            {
               EmpRefId:emprefid,
               hCompanyID:$("#hCompanyID").val(),
               hBranchID:$("#hBranchID").val(),
               hEmpRefId:$("#hEmpRefId").val(),
               hUserRefId:$("#hUserRefId").val()
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     eval(data);
                  } catch (e) {
                      if (e instanceof SyntaxError) {
                          alert(e.message);
                      }
                  }
               }
            });
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("INDIVIDUAL DEVELOPMENT PLAN"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-sm-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-sm-9" style="padding: 10px;">
                        <div class="row" style="padding-left: 5px;">
                           <button type="button" class="btn-cls4-sea" id="btnsaveDevPlan">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              &nbsp;Save
                           </button>
                           <input type="hidden" name="fn" value="saveDevPlan">
                        </div>
                        <div class="panel-top margin-top">
                           INDIVIDUAL DEVELOPMENT PLAN
                        </div>
                        <div class="panel-mid" style="padding: 15px;">
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="row" style="border: 1px solid black; padding: 10px;">
                                    <div class="col-sm-12">
                                       <div class="form-group">
                                          <div class="row">
                                             <div class="col-sm-4">
                                                <label>EMPLOYEE NAME</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" id="employeename" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Current Position</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" id="PositionRefId" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Salary Grade</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" id="SalaryGradeRefId" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Years in Position</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Years in Agency</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Two-Year Period</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Division</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" id="DivisionRefId" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Office</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" id="OfficeRefId" readonly>
                                             </div>
                                          </div>
                                          <?php bar();?>
                                          <div class="row margin-top">
                                             <div class="col-sm-12">
                                                <label>No further development is desired or required for this year/s</label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-4">
                                                <input type="radio" class="" name="sint_desiredyr" value="1" id="yr1">&nbsp;&nbsp;&nbsp;<label for="yr1">Year 1</label>
                                             </div>
                                             <div class="col-sm-4">
                                                <input type="radio" class="" name="sint_desiredyr" value="2" id="yr2">&nbsp;&nbsp;&nbsp;<label for="yr2">Year 2</label>
                                             </div>
                                             <div class="col-sm-4">
                                                <input type="radio" class="" name="sint_desiredyr" value="3" id="both">&nbsp;&nbsp;&nbsp;<label for="both">Both years</label>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-4">
                                                <label>Supervisor's Name</label>
                                             </div>
                                             <div class="col-sm-8">
                                                <input type="text" class="form-input" name="char_supervisor">
                                             </div>
                                          </div>
                                          <?php bar();?>
                                          <div class="row margin-top">
                                             <div class="col-sm-12">
                                                <label>Purpose</label>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-sm-12">
                                                <div class="row margin-top">
                                                   <input type="checkbox" class="" id="p1" name="sint_P1" value="0">&nbsp;
                                                   <label for="p1">To meet the competencies of current position</label>
                                                </div>
                                                <div class="row margin-top">
                                                   <input type="checkbox" class="" id="p2" name="sint_P2" value="0">&nbsp;
                                                   <label for="p2">To increase the level of competencies of current position</label>
                                                </div>
                                                <div class="row margin-top">
                                                   <input type="checkbox" class="" id="p3" name="sint_P3" value="0">&nbsp;
                                                   <label for="p3">To meet the competencies of the next higher position</label>
                                                </div>
                                                <div class="row margin-top">
                                                   <input type="checkbox" class="" id="p4" name="sint_P4" value="0">&nbsp;
                                                   <label for="p4">To acquire new competencies accross different functions/position</label>
                                                </div>
                                                <div class="row margin-top">
                                                   <input type="checkbox" class="" id="p5" name="sint_P5" value="0">&nbsp;
                                                   <label for="p5">Others, please specify</label>
                                                   <input type="text" class="form-input" name="p5_exp" id="p5_exp" disabled>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="row" style="padding: 10px;">
                                    <div class="col-sm-12">
                                       <div class="row margin-top text-center" style="background: gray;color:white;">
                                          <label>COMPETENCY ASSESSMENT AMD DEVELOPMENT PLAN</label>
                                       </div>
                                       <div class="row margin-top">
                                          <label>Target Competency (1)</label>
                                          <input type="text" class="form-input">
                                       </div>
                                       <div class="row margin-top">
                                          <label>Priority for IDP (2)</label>
                                          <input type="text" class="form-input">
                                       </div>
                                       <div class="row margin-top">
                                          <label>Specific Behavioral Indicators NOT Considently Demonstrated (3)</label>
                                          <input type="text" class="form-input">
                                       </div><div class="row margin-top">
                                          <label>Development Activity (4)</label>
                                          <input type="text" class="form-input">
                                       </div>
                                       <div class="row margin-top">
                                          <label>Support Needed (5)</label>
                                          <input type="text" class="form-input">
                                       </div>
                                       <div class="row margin-top">
                                          <label>Trainer/Provider (6)</label>
                                          <input type="text" class="form-input">
                                       </div><div class="row margin-top">
                                          <label>Schedule or Completion Date (7)</label>
                                          <input type="text" class="form-input">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                        </div>
                     </div>

                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



