<?php
   function setVal($value) {
      if ($value == 0) {
         return "&nbsp;";
      } else {
         return "<span style='color:white;'>$value</span>";
      }
   }
?>
<table border="1" style="width: 100%; border-collapse: collapse;">
   <tr style="height: <?php echo mysqli_num_rows($rsEmployees); ?>px;">
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $M_Si; ?>px; background: #00477e;">
            <?php echo setVal($M_Si); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $F_Si; ?>px; background: #a6001a;">
            <?php echo setVal($F_Si); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $M_An; ?>px; background: #00477e;">
            <?php echo setVal($M_An); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $F_An; ?>px; background: #a6001a;">
            <?php echo setVal($F_An); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $M_Ma; ?>px; background: #00477e;">
            <?php echo setVal($M_Ma); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $F_Ma; ?>px; background: #a6001a;">
            <?php echo setVal($F_Ma); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $M_Wi; ?>px; background: #00477e;">
            <?php echo setVal($M_Wi); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $F_Wi; ?>px; background: #a6001a;">
            <?php echo setVal($F_Wi); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $M_Se; ?>px; background: #00477e;">
            <?php echo setVal($M_Se); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $F_Se; ?>px; background: #a6001a;">
            <?php echo setVal($F_Se); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $M_Ot; ?>px; background: #00477e;">
            <?php echo setVal($M_Ot); ?>
         </div>
      </td>
      <td valign="bottom" style="padding: 1px 1px 0px 1px;">
         <div class="text-center" style="width: 100%; height: <?php echo $F_Ot; ?>px; background: #a6001a;">
            <?php echo setVal($F_Ot); ?>
         </div>
      </td>
   </tr>
   <tr>
      <th colspan="2" class="text-center" style="width: 16.6%;">Single</th>
      <th colspan="2" class="text-center" style="width: 16.6%;">Annulled</th>
      <th colspan="2" class="text-center" style="width: 16.6%;">Married</th>
      <th colspan="2" class="text-center" style="width: 16.6%;">Widowed</th>
      <th colspan="2" class="text-center" style="width: 16.6%;">Separated</th>
      <th colspan="2" class="text-center" style="width: 16.6%;">Other</th>
   </tr>
</table>