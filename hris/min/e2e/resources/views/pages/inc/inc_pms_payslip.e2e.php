<?php
	$emprefid = $_GET["hEmpRefId"];
	$hris_sql = "SELECT * FROM employees WHERE RefId = '$emprefid'";
	$hris_rs  = mysqli_query($conn,$hris_sql);
	
	$BASIC 					= 0;
	$PERA_id 				= 0;
	$PERA 					= 0;
	$Overtime 				= 0;
	$Uniform_Allowance 		= 0;
	$CNA_PEI 				= 0;
	$Opt_Ins 				= 0;
	$Genesis 				= 0;
	$Hosp_Plus 				= 0;
	$Longevity_Pay 			= 0;
	$Hazard_Pay 			= 0;
	$SALA 					= 0;
	$RATA 					= 0;
   	$Division            = "";
   	$PositionItem        = "";
   	$Position            = "";
   	$Office              = "";
	if ($hris_rs) {
		$hris_row 	= mysqli_fetch_assoc($hris_rs);
		$AgencyId 	= $hris_row["AgencyId"];
		$FirstName 	= $hris_row["FirstName"];
		$LastName 	= $hris_row["LastName"];
		$MiddleName = $hris_row["MiddleName"];
		$FullName   = $LastName.", ".$FirstName." ".$MiddleName;
		$pms_sql    = "SELECT * FROM pms_employees WHERE employee_number = '$AgencyId'";
		$pms_rs     = mysqli_query($pms_conn,$pms_sql);
		$company    = $hris_row["CompanyRefId"];
		if ($pms_rs) {
			$pms_row = mysqli_fetch_assoc($pms_rs);
			$pms_refid = $pms_row["id"];
			/*------------------------------------------------*/
			$pms_salary_info = pms_FindFirst("pms_salaryinfo","WHERE employee_id = '$pms_refid'");
			if ($pms_salary_info) {
				switch ($company) {
					case '21':
						$BASIC = $pms_salary_info["basic_amount_one"] + $pms_salary_info["basic_amount_two"];
						break;
					
					default:
						$BASIC = $pms_salary_info["salary_new_rate"];
						break;
				}
				
			}
			/*------------------------------------------------*/
			$PERA_refid = pms_FindFirst("pms_benefits","WHERE name = 'PERA'");
			if ($PERA_refid) {
				$PERA_id = $PERA_refid["id"];
			}
			/*------------------------------------------------*/
			/*------------------------------------------------*/
			/*------------------------------------------------*/
			if ($PERA_id > 0) {
				$pms_benefits_info = pms_FindFirst("pms_benefitsinfo","WHERE employee_id = '$pms_refid' AND benefit_id = '$PERA_id'");
				if ($pms_benefits_info) {
					$PERA = $pms_benefits_info["benefit_amount"];
				}
			}
			/*------------------------------------------------*/
			/*------------------------------------------------*/
			$pms_payroll_info = pms_FindFirst("pms_payroll_information","WHERE employee_id = '$pms_refid'");
			if ($pms_payroll_info) {
				$gsis_contribution 			= $pms_payroll_info["gsis_contribution"];
				$philhealth_contribution 	= $pms_payroll_info["philhealth_contribution"];
				$pagibig_contribution 		= $pms_payroll_info["pagibig_contribution"];
				$tax_contribution 			= $pms_payroll_info["tax_contribution"];
			} else {
				$gsis_contribution 			= 0;
				$philhealth_contribution 	= 0;
				$pagibig_contribution 		= 0;
				$tax_contribution 			= 0;
			}

			$gsis_loan 		= pms_GetLoan($pms_refid,"CONSOLOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$pagibig_mpl 	= pms_GetLoan($pms_refid,"MULTI PURPOSE LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$policy 			= pms_GetLoan($pms_refid,"POLICY LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$WF_loan			= pms_GetLoan($pms_refid,"WELFARE FUND LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			$WASSSLAI_loan	= pms_GetLoan($pms_refid,"WASSSLAI LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
         	$EL_loan       = pms_GetLoan($pms_refid,"GSIS EMERGENCY LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
         	$Optional_loan = pms_GetLoan($pms_refid,"OPTIONAL LIFE","pms_loans","pms_loansinfo","loan_id","loan_amortization");
         	$Educ_loan     = pms_GetLoan($pms_refid,"EDUCATION ASST LOAN","pms_loans","pms_loansinfo","loan_id","loan_amortization");
			//$MPLP_loan		= pms_GetLoan($pms_refid,"WASSSLAI LOAN","pms_loans","pms_loaninfo","loan_id","loan_amortization");
			$MPLP_loan     = 0;



			$WF_Contribution 	=  pms_GetLoan($pms_refid,"WELFARE FUND","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$WASSSLAI_Dep 		=  pms_GetLoan($pms_refid,"WASSSLAI Capital Contribution","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$Union_Dues 		=  pms_GetLoan($pms_refid,"UNION DUES","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$MP_Coop 			=  pms_GetLoan($pms_refid,"MULTI-PURPOSE COOPERATIVE CONTRIBUTION","pms_deductions","pms_deductioninfo","deduction_id","deduct_amount");
			$Agency_Fee = 0;
		}
	}
	$Total_Earnings = $BASIC + $PERA + $Overtime + $CNA_PEI;
	$Total_Deductions = $gsis_contribution + $pagibig_contribution + $philhealth_contribution + $Opt_Ins + $Genesis + $Hosp_Plus +
								$gsis_loan + $pagibig_mpl + $policy + $WF_Contribution + $WF_loan + $WASSSLAI_Dep + $WASSSLAI_loan +
								$tax_contribution + $MPLP_loan + $Union_Dues + $MP_Coop + $Agency_Fee;
	$NET_Earnings = $Total_Earnings - $Total_Deductions;
?>