<?php
	include_once 'conn.e2e.php';
	$bio_arr_ovp = array();
	$biometricsID = intval($biometricsID);
	$sql_ovp = "SELECT * FROM agent_log";
	$rs_ovp  = mysqli_query($bio_conn,$sql_ovp);
	$checking = 0;
	if ($rs_ovp) {
		while ($row_ovp = mysqli_fetch_assoc($rs_ovp)) {
			$UserID = $row_ovp["UserID"];
			$UserID = intval($UserID);
			if ($biometricsID == $userID) {
				$checking++;
			}
		}
	}
	if ($checking > 0) {
		$count_ovp = 0;
		$where_ovp = "WHERE UserID = '$biometricsID' AND (EventTrigger = 'IN' OR EventTrigger = 'OUT')";
		$where_ovp .= " AND Timestamp_ACTAtek BETWEEN '".$month_start."' AND '".$month_end."'";
		$new_sql_ovp = "SELECT * FROM agent_log ".$where_ovp." ORDER BY Timestamp_ACTAtek";
		$new_rs_ovp	 = mysqli_query($ovp_conn,$new_sql_ovp);
		if ($new_rs_ovp) {
			while ($new_row_ovp = mysqli_fetch_assoc($new_rs_ovp)) {
				$date = date("Y-m-d",strtotime($new_row_ovp["Timestamp_ACTAtek"]));
				$bio_arr_ovp[$biometricsID][$date][$count_ovp] = $new_row_ovp["Timestamp_ACTAtek"];
			}
		}
	}
	foreach ($bio_arr_ovp as $key => $value) {
		foreach ($value as $nkey => $nvalue) {
			$first 	= reset($nvalue);
			$last 	= end($nvalue);
			$utc_f 	= strtotime($first);
   			$val_f 	= get_today_minute($utc_f);

   			$utc_l 	= strtotime($last);
   			$val_l 	= get_today_minute($utc_l);
			
			$utc 	= strtotime($first);
   			$d 		= date("Y-m-d",$utc);
			if (isset($arr["ARR"][$d]["AttendanceDate"])) {
				if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
		    		if ($arr["ARR"][$d]["TimeIn"] == "") {
			    		$arr["ARR"][$d]["TimeIn"] = $val_f;
			    		$arr["ARR"][$d]["UTC"] = $utc_f;
			    	}
			    	if ($arr["ARR"][$d]["TimeOut"] == "") {
			    		$arr["ARR"][$d]["TimeOut"] = $val_l;
			    		$arr["ARR"][$d]["UTC"] = $utc_l;
			    	}	
		    	}	
			}		
		}
	}
	
?>