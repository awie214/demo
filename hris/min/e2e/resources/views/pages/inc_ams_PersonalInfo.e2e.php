<div class="row">
   
   <div class="col-xs-2 txt-center">
      <img src="<?php echo img("nopic.png"); ?>"
         style="margin-bottom:3px;border:1px solid #aaa;width:1.2in;height:1.5in;"/>
      <br>
      <button type="button"
              class="btn btn-primary btn-sm"
              id="btnUPLOADPIC" name="btnUPLOADPIC"
              title="Upload Picture">
               <i class="fa fa-upload"></i>&nbsp;&nbsp;Upload Picture
      </button>
   </div>
   <div class="col-xs-10">
      <div class="row">
         <div class="col-xs-6">
            <div class="row">
               <div class="form-group">
                  <div class="col-xs-4 text-right">
                     <label class="control-label label" for="inputs">LAST NAME:</label>
                  </div>
                  <div class="col-xs-8">
                     <input class="form-input saveFields--" type="text" id="" name="char_LastName" value="" autofocus>
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="form-group">
                  <div class="col-xs-4 text-right">
                     <label class="control-label label" for="inputs">FIRST NAME:</label>
                  </div>
                  <div class="col-xs-8">
                     <input class="form-input saveFields--" type="text" id="" name="char_FirstName" value="">
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="form-group">
                  <div class="col-xs-4 text-right">
                     <label class="control-label label" for="inputs">MIDDLE NAME:</label>
                  </div>
                  <div class="col-xs-8">
                     <input class="form-input saveFields--" type="text" id="" name="char_MiddleName" value="">
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="form-group">
                  <div class="col-xs-4 text-right">
                     <label class="control-label label" for="inputs">EXT. NAME:</label>
                  </div>
                  <div class="col-xs-5">
                     <input class="form-input saveFields--" type="text" id="" name="char_ExtName" value="">
                  </div>
               </div>
            </div>
            <div class="row margin-top">
               <div class="form-group">
                  <div class="col-xs-4 text-right">
                     <label class="control-label label" for="inputs">DATE OF BIRTH:</label>
                  </div>
                  <div class="col-xs-5">
                     <input class="form-input date-- saveFields--" type="text" id="BirthDate" name="date_BirthDate" value="">
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-6">
            <div class="row">
               <div class="col-xs-8 txt-right">
                  <label class="control-label" for="inputs">First Day of Service:</label>
               </div>
               <div class="col-xs-4">
                  <input class="form-input date-- saveFields--" type="text" id="HiredDate" name="date_HiredDate" value="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-8 txt-right">
                  <label class="control-label" for="inputs">Assumption to Duty:</label>
               </div>
               <div class="col-xs-4">
                  <input class="form-input date-- saveFields--" type="text" id="" name="date_AssumptionDate" value="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-8 txt-right">
                  <label class="control-label" for="inputs">Resigned/Retired Date:</label>
               </div>
               <div class="col-xs-4">
                  <input class="form-input date-- saveFields--" type="text" id="" name="date_ResignedDate" value="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-8 txt-right">
                  <label class="control-label" for="inputs">Re-employment:</label>
               </div>
               <div class="col-xs-4">
                  <input class="form-input date-- saveFields--" type="text" id="" name="date_RehiredDate" value="">
               </div>
            </div>
            <?php bar(); ?>
            <div class="row margin-top text-center">
               <label class="control-label" for="inputs">
                  FOR CONTRACT OF SERVICE/JOB ORDER
               </label>
            </div>
            <div class="row margin-top">
               <div class="col-xs-8 txt-right">
                  <label class="control-label" for="inputs">Assumption to Duty:</label>
               </div>
               <div class="col-xs-4">
                  <input class="form-input date-- saveFields--" type="text" id="" name="date_JOStartDate" value="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-8 txt-right">
                  <label class="control-label" for="inputs">End of Contract:</label>
               </div>
               <div class="col-xs-4">
                  <input class="form-input date-- saveFields--" type="text" id="" name="date_JOEndDate" value="">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-8 txt-right">
                  <label class="control-label" for="inputs">Renewal Date:</label>
               </div>
               <div class="col-xs-4">
                  <input class="form-input date-- saveFields--" type="text" id="" name="date_RenewalDate" value="">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>