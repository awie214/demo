<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $count++;
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="7">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="7">
                     <div class="row">
                        <div class="col-xs-12">
                           This Certificate entitles Mr./Ms. <u><?php echo $FullName; ?></u> to __________ of Compensatory Overtime Credits.
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-right">
                           <b>ROBERT O. DIZON</b>
                           <br>
                           Executive Director   
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           Date Issued: <u><?php echo date("F d, Y",time()); ?></u>
                           <br>
                           Valid Until: <u><?php echo "December 31, ".date("Y",time()); ?></u>
                        </div>
                     </div>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th colspan="7">DETAILS OF COMPENSATORY OVERTIME CREDITS</th>
               </tr>
               <tr class="colHEADER">
                  <th colspan="3">"No. of Hours of Earned<br>COCs/Beginning Balance"</th>
                  <th>Date of CTO</th>
                  <th>Used COCs</th>
                  <th>Remaining COCs</th>
                  <th>Remarks</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  for ($i=1; $i <= 10; $i++) { 
                     echo '<tr>
                              <td colspan="3">&nbsp;</td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                           </tr>   
                     ';
                  }
               ?>
               <tr>
                  <td colspan="2" class="text-center"><b>TOTAL</b></td>
                  <td class="text-right">0</td>
                  <td class="text-right"></td>
                  <td class="text-right">0</td>
                  <td class="text-right">0</td>
                  <td class="text-right"></td>

               </tr>
            </tbody>
         </table>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               * 40 hours is the allowed maximum overtime per month and should not exceed to 120 hours of expendable balance
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-6">
               Approved by:
            </div>
            <div class="col-xs-6">
               Claimed:
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-6 text-center">
               <b>ROBERT O. DIZON</b>
               <br>
               Executive Director
               <br>
               <br>
               <u><?php echo date("F d, Y",time()); ?></u>
               <br>
               Date
            </div>
            <div class="col-xs-6 text-center">
               <b>JELLY N. ORTIZ, DPA</b>
               <br>
               Chief, FAD-AGSS
               <br>
               <br>
               <u><?php echo date("F d, Y",time()); ?></u>
               <br>
               Date
            </div>
         </div>
         <?php
               }
            }
         ?>
      </div>
   </body>
</html>