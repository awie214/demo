<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 10";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <br><br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-8">
                     <label>Division:&nbsp;____________________________________________ </label>
                  </div>
                  <div class="col-xs-4">
                     <label>Date of Filing:&nbsp;_____________________</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 text-center">
                     <label>EMPLOYEE/S TO RENDER OT SERVICES</label>
                  </div>
                  <div class="col-xs-3 text-center">
                     <label>OT PERIOD</label>
                  </div>
                  <div class="col-xs-3 text-center">
                     <label>TIME</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6 text-center">
                     _____________________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 text-center">
                     _____________________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6 text-center">
                     _____________________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
                  <div class="col-xs-3 text-center">
                     ____________________
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <label>PURPOSE/EXPECTED OUTPUT:</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12" style="padding: 5px;">
                     <div class="row">
                        <div class="col-xs-12" style="border-bottom: 1px solid black;">&nbsp;</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12" style="border-bottom: 1px solid black;">&nbsp;</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12" style="border-bottom: 1px solid black;">&nbsp;</div>
                     </div>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <label>ENTITLED REMUNERATION:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox" name="" disabled>&nbsp;COC
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox" name="" disabled>&nbsp;OT Pay
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox" name="" disabled>&nbsp;None 
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <i><b>Note:</b>  Only personnel not receiving honoraria due to assignment to any government special projects are entitled to CTO/OT Pay.</i>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4 text-center">
                     <label>Requested by:</label>
                  </div>
                  <div class="col-xs-4 text-center">
                     <label>Recommending Approval</label>
                  </div>
                  <div class="col-xs-4 text-center">
                     <label>Approved by:</label>
                  </div>
               </div>  
               <div class="row margin-top">
                  <div class="col-xs-4 text-center">
                     _____________________________
                  </div>
                  <div class="col-xs-4 text-center">
                     ____________________
                  </div>
                  <div class="col-xs-4 text-center">
                     ____________________
                  </div>
               </div> 
            </div>
         </div>
         <?php
            }
         ?>
      </div>
   </body>
</html>