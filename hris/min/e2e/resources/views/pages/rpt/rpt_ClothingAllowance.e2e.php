<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="10" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>   
               <tr class="colHEADER">
                  <th>ID</th>
                  <th>EMPLOYEE NAME</th>
                  <th>POSITION</th>
                  <th>STATUS OF<br>APPOINTMENT</th>
                  <th>ASSUMPTION</th>
                  <th>TAX CODE</th>
                  <th>JG</th>
                  <th>6 MONTHS</th>
                  <th>&nbsp;</th>
                  <th>AMOUNT</th>
               </tr>
            </thead>
            <tbody>
                  <?php for ($j=1;$j<=60;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
                  <tr>
                     <td></td>
                     <td colspan="8">Total No. Of Employees</td>
                     <td>TOTAL AMOUNT</td>
                  </tr>         
            </tbody>
            <tfoot>
               <tr>
                     <td colspan="10">
                        <br><br>
                        <p>
                           <div class="row">
                              <div class="col-xs-8">
                                 This is to cerfity that the above mentioned PCC Officers and employees were not included in the 1st batch  of Uniform/Clothing Allowance given last March 2017. They have rendered 6 months and are expected to render another 6 months of service  subject to refund if the government service is not fulfilled.   
                              </div>
                              <div class="col-xs-4 text-right">
                                 Approved for Payment:
                              </div>
                              
                           </div>
                           <?php spacer(20); ?>
                           <div class="row">
                              <div class="col-xs-8">
                                 <label>ANTONIA LYNNELY L. BAUTISTA</label><br>
                                 Chief Admin Officer, HRDD
                              </div>
                              <div class="col-xs-4 text-right">
                                 <label>GWEN GRECIA-DE VERA</label><br>
                                 Executive Director
                              </div>
                              
                           </div>
                        </p>     
                     </td>
                  </tr>
               <tr>
                  <td colspan="10">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>