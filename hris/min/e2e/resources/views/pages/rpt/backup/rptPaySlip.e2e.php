<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1) ?></u> 2017</p>
         <div style="">
            <table>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">NAME:Juan Dela Cruz</td>
                  <td colspan="3"></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">EMPLOYEEE ID:002</td>
                  <td colspan="3"></td>
               </tr>
               <tr>
                  <td colspan="6">&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">Basic Salary:</td>
                  <td>&nbsp;</td>
                  <td>P 10 484.00</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">Taxable Income:</td>
                  <td>&nbsp;</td>
                  <td>P 10 484.00</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td colspan="6">&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="5"><b>Allowances</b></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>PERA</td>
                  <td>&nbsp;</td>
                  <td>P 1 545.00</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Gross Compensation</td>
                  <td>&nbsp;</td>
                  <td>P 9 928.00</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td colspan="6">&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td colspan="5"><b>Deductions</b></td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Witholding Tax:</td>
                  <td>&nbsp;</td>
                  <td>P 688.70</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>GSIS Contribution</td>
                  <td>&nbsp;</td>
                  <td>P 754.43</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>PAGIBIG:</td>
                  <td>&nbsp;</td>
                  <td>P 200.00</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>PHILHEALTH</td>
                  <td>&nbsp;</td>
                  <td>P 125.00</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Loans:</td>
                  <td>&nbsp;</td>
                  <td>P 200.00</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Abs/Late/UT:</td>
                  <td>&nbsp;</td>
                  <td>P 2 465.45</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Total Deductions:</td>
                  <td>P 4 433.58</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Net Pay:</td>
                  <td>P 6 050.42</td>
                  <td>&nbsp;</td>
               </tr>
            </table>
         </div>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>