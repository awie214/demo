<?php
   //session_start();
   //include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   
   include "incRptSortBy.e2e.php";
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);

   if ($dbg) {
      echo $whereClause;
   }
   $file = "FOR AGENCY REMITTANCE ADVICE";
?>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <label>Agency Name:&nbsp;<?php //echo $row['']; ?></label>
         </div>
         <div class="row margin-top">
            <label>Agency BP Number:&nbsp;<?php //echo $row['']; ?></label>
         </div>
         <div class="row margin-top">
            <label>FORM E. List of Employees with changes / correction in their Personal Data.</label>
         </div>
         <?php
            $errmsg = "";
            rptHeader("FOR AGENCY REMITTANCE ADVICE");
            if ($rs && $errmsg == "")
            {
         ?>
         <table border="1" width="80%">
            <tr>
               <td nowrap class="center--">Ref Id</td>
               <td nowrap class="center--">Member BP Number</td>
               <td nowrap class="center--">Employee Name</td>
               <td nowrap class="center--" style="width:100px;">Mailing/Address/Zip Code</td>
               <td nowrap class="center--">Cellular Phone No.</td>
               <td nowrap class="center--">Email Address</td>
               
               <td nowrap class="center--">Civil Status</td>
               <td nowrap class="center--">Date of Birth</td>
               <td nowrap class="center--">Place of Birth</td>
               
               <td nowrap class="center--">Position / Title</td>
               <td nowrap class="center--">Status of Employment</td>
               
            </tr>
            <?php
               while ($row = mysqli_fetch_assoc($rs)) {
            ?>
               <tr>
                  <td nowrap class="center--"><?php echo $row['RefId'];?></td>
                  <td nowrap>Member BP Number</td>
                  <td nowrap><?php echo $row['LastName'].",".$row['FirstName']." ".$row['MiddleName']." ".$row['ExtName']; ?></td>
                  <td nowrap><?php echo $row['ResiHouseNo']." ".$row['ResiStreet']." ".$row['ResiBrgy']; ?></td>
                  <td nowrap class="center--"><?php echo $row['MobileNo']; ?></td>
                  <td nowrap class="center--"><?php echo $row['EmailAdd']; ?></td>
                  
                  <td nowrap class="center--"><?php 
                  $CivilStat = $row['CivilStatus']; 
                  switch ($CivilStat) {
                     case "Si":
                        echo 'Single';
                     break;
                     case "Ma":
                        echo 'Married';
                     break;
                     case "An":
                        echo 'Annulled';
                     break;
                     case "Wi":
                        echo 'Widowed';
                     break;
                     case "Se":
                        echo 'Separated';
                     break;
                     case "Ot":
                        echo 'Others';
                     break;
                  }
                  ?></td>
                  <td nowrap class="center--"><?php echo $row['BirthDate']; ?></td>
                  <td nowrap class="center--"><?php echo $row['BirthPlace']; ?></td>
                  <td nowrap>
                     <?php 
                        $Position = getRecord("employeesworkexperience",$row['RefId'],"PositionRefId");
                        echo getRecord("position","$Position","Name");
                     ?>
                  </td>
                  <td nowrap class="center--">
                     <?php 
                        if ($row['Inactive'] = 1) {
                           echo "Active";
                        }else {
                           echo "Inactive";
                        } 
                     ?>
                  </td>
               </tr>   
            <?php
               }
               echo "RECORD COUNT : ".mysqli_num_rows($rs);
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
      </div>
      <p class="MsoNormal margin-top" style='margin-bottom:0cm;margin-bottom:.0001pt;
         text-align:center;line-height:normal'><span style='font-size:10.0pt;
         font-family:"Arial",sans-serif'>Issue No. 01, Rev No. 0, (16 August 2016),
         FM-GSIS-OPS-UMR-01</span></p>
      <?php rptFooter();?>
   </body>
</html>