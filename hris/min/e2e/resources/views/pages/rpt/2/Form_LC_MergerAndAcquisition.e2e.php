<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
?>
<!DOCTYPE html>
<html>
	<head>
   	<?php include_once $files["inc"]["pageHEAD"]; ?>
   	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   	<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
	</head>
	<body>
   	<div class="container-fluid rptBody">

            <?php
               $rs = SelectEach("employees",$whereClause);
               if (mysqli_num_rows($rs)) {
                  while ($row = mysqli_fetch_assoc($rs)) {
                     $LastName       = $row["LastName"];
                     $FirstName      = $row["MiddleName"];
                     $MiddleName     = $row["FirstName"];
                     $FullName       = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];
            ?>

   		<table>
   			<thead>
               <!--<tr>
                  <th style="width: 4.54cm;"></th>
                  <th style="width: 8.85cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th style="width: 4.00cm;"></th>
                  <th></th>
               </tr>-->
   				<tr>
                  <th colspan="11" align="center" style="text-align:center;">
                  <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <?php spacer(30); ?>
                  </th>
               </tr> 
               <tr>
                  <th colspan="5" class="text-left">
                     <?php echo ("$FullName");?>
                     <br>
                     Employee Name
                     <br>
                     Appointment: 
                  </th>
                  <th colspan="3"></th>
                  <th colspan="3" class="text-right">
                     _______________
                     <br>
                     1st Day of Service
                  </th>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">Period</th>
                  <th rowspan="2">Particular</th>
                  <th colspan="4">Vacation Leave</th>
                  <th colspan="4">Sick Leave</th>
                  <th rowspan="2">Remarks</th>
               </tr>
               <tr class="colHEADER">
                  <th>Earned</th>
                  <th>Absent/<br>Undertime<br>W/P</th>
                  <th>Balance</th>
                  <th>Absent/<br>Undertime<br>WO/P</th>
                  <th>Earned</th>
                  <th>Absent/<br>Undertime<br>W/P</th>
                  <th>Balance</th>
                  <th>Absent/<br>Undertime<br>WO/P</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  for ($i=1; $i <=10 ; $i++) { 
                     echo '
                        <tr>
                           <td>&nbsp;</td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                     ';
                  }
               ?>
            </tbody>
         </table>
      <?php
            }
         }
      ?>
      </div>
   </body>
</html>