<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="7" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>   
               <tr class="colHEADER">
                  <th>ID</th>
                  <th>EMPLOYEE NAME</th>
                  <th>POSITION/DESIGNATION</th>
                  <th>OFFICE</th>
                  <th>COMMUNICATIONS EXPENSE ALLOTMENT</th>
                  <th>SIGNATURE</th>
                  <th>REMARKS</th>
               </tr>
               <tr class="colHEADER">
                 <th colspan="7" rowspan="2">
                   We hereby certify to have incurred Communication expenses in the performance of our official functions in our respective office/divisions for the month of NOVEMBER 2017 as per Commission Resolution No. 80 series of 2016.
                 </th>
               </tr>
            </thead>
            <tbody>
               <?php 
                   $rs = SelectEach("empinformation","WHERE EmployeesRefId = 100000 GROUP BY RefId LIMIT 60");
                   $i = 0;
                   if ($rs){
                      while($row = mysqli_fetch_assoc($rs)){
                   $i++;
                  ?>
                  <tr>
                     <td><?php echo $row["EmployeesRefId"]; ?></td>
                     <td>
                      <?php
                        $result = mysqli_query($conn,"SELECT * FROM employees WHERE RefId =".$row["EmployeesRefId"]);
                        if (mysqli_num_rows($result) > 0 ){
                          while($data = mysqli_fetch_assoc($result)){
                            echo $data["LastName"].", ".$data["FirstName"];
                          }
                        }
                      ?>
                     </td>
                     <td><?php echo getRecord("position",$row["PositionRefId"],"Name"); ?></td>
                     <td><?php echo getRecord("office",$row["OfficeRefId"],"Name"); ?></td>
                     <td align="right">P 10 000.000</td>
                     <td align="center"><?php echo $i;?></td>
                     <td align="center">_________________</td>
                  </tr>
                  <?php 
                        } 
                     } else {
                        for ($a=1; $a<=30; $a++) {
                  ?>
                     <tr>
                        <td><?php echo $a;?></td>
                        <td>
                           JUAN DELA CRUZ
                        </td>
                        <td></td>
                        <td></td>
                        <td align="right">P 10 000.000</td>
                        <td align="center"><?php echo $i;?></td>
                        <td align="center">_________________</td>
                     </tr>
                  <?php
                        }
                     }
                  ?>
               <tr>
                 <td colspan="4" align="right"><label>TOTAL</label></td>
                 <td></td>
                 <td></td>
                 <td colspan="3"></td>
               </tr>
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="7">
                    <br><br>
                    <div class="row">
                      <div class="col-xs-6">
                        <div class="row">Certified Correct:</div>
                        <div class="row margin-top"><b>ANTONIA LYNNELY L. BAUTISTA</b></div>
                        <div class="row margin-top">Chief Admin Officer, HRDD</div>
                        <div class="row margin-top">
                          Certified: Supporting documents complete and proper, and cash available in the amount of Php _____________________.
                        </div>
                        <div class="row margin-top">
                          <b>CAROLYN V. AQUINO</b>
                        </div>
                        <div class="row margin-top">
                          Accountant III, FPMO
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="row">Approved for Payment:</div>
                        <div class="row margin-top"><b>GWEN GRECIA-DE VERA</b></div>
                        <div class="row margin-top">Executive Director</div>
                        <div class="row margin-top">
                          Certified: Each employee whose name appears on the payroll has been paid the amount as indicated opposite his/her name.
                        </div>
                        <div class="row margin-top">
                          <b>FLERIDA J. GIRON</b>
                        </div>
                        <div class="row margin-top">
                          Cashier III, ALO
                        </div>
                      </div>

                    </div>
                  </td>
                </tr>
               <tr>
                  <td colspan="7">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>