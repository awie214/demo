<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include_once 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
   $rsEmployees = SelectEach("employees",$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) { echo "DBG >> ".$whereClause; }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="8" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                     <?php spacer(10); ?>                     
                     <u>__________NAME OF AGENCY___________</u>

                     <?php spacer(20); ?>
                  </th>
               </tr>  
               <tr class="colHEADER">
                  <th>Employee Name</th>
                  <th>Sex</th>
                  <th>Date And Place of Birth</th>
                  <th>Occupation Category</th>
                  <th>Position</th>
                  <th>Agency</th>
                  <th>Date And Place of Exam</th>
                  <th>Eligibility/Rating</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  while ($row = mysqli_fetch_assoc($rsEmployees)) {
                        $EmployeesRefId = $row["RefId"];
                        $CompanyRefId   = $row["CompanyRefId"];
                        $BranchRefId    = $row["BranchRefId"];
                        $where  = "WHERE CompanyRefId = $CompanyRefId";
                        $where .= " AND BranchRefId = $BranchRefId";
                        
                        $Sex = $row["Sex"];
                           if ($Sex == "M") {
                              $Sex = "Male";
                           } else if ($Sex == "F") {
                              $Sex = "Female";
                           }
                        $BirthDate  = $row["BirthDate"];
                        $BirthPlace = $row["BirthPlace"];

                        $where .= " AND EmployeesRefId = $EmployeesRefId";
                        $empinfo_row = FindFirst("empinformation",$where,"*");
                        if ($empinfo_row) {
                           $Position = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                           $Agency   = getRecord("Agency",$empinfo_row["AgencyRefId"],"Name");  
                        } else
                           $Position = "";
                           $Agency   = "";
                ?>
                  <tr>
                     <td><?php echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"]; ?></td>
                     <td>
                     <?php 
                        echo $Sex; 
                     ?>
                     </td>
                     <td>
                        <?php
                              if ($BirthDate != "" && $BirthPlace != "") {
                                 echo $BirthDate." / ".$BirthPlace;
                              } else {
                                 if ($BirthDate != "") {
                                    echo $BirthDate;
                                 }
                                 if ($BirthPlace != "") {
                                    echo $BirthPlace;
                                 }
                              }
                              
                           ?>
                     </td>
                     <td></td>
                     <td><?php echo $Position; ?></td>
                     <td>
                     <?php
                        echo $Agency;
                     ?>
                     </td>
                     <td></td>
                     <td></td>
                  </tr>
               <?php } ?>
            </tbody>
         </table>
         <?php spacer(50); ?>
         <table>
            <thead>
               <tr class="colHEADER">
                  <th colspan="8" class="txt-center">
                     DECISION INFORMATION
                  </th>
               </tr>
               <tr class="colHEADER">
                  <th>DOCUMENT TYPE/ NO./ DATE</th>
                  <th>SIGNATORY</th>
                  <th>POSITION</th>
                  <th>AGENCY</th>
                  <th>OFFENSE</th>
                  <th>PENALTY</th>
                  <th colspan="2">REMARKS</th>
               </tr>
            </thead>
            <tbody>
               <?php for ($j=1;$j<=10;$j++) { ?>
                  <tr>
                     <td>&nbsp;</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td colspan="2">&nbsp;</td>
                  </tr>
               <?php } ?>
               <tr>
                  <td colspan="8">
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <b>DECISION INFORMATION</b> - Decision/Resolution/Order issued to bar certain individuals from entering government service and taking civil service examinations
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           I hereby certify that the Decisions/Resolutions/Orders enumerated are executory.
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           <div class="row txt-center">
                              Prepared by: ___________________________________________
                              <br>
                              Chief/Head of Division
                              <br>
                              (Signature over Printed Name)
                              <br><br>
                              DATE:_______________________________________
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="row txt-center">
                              Certified Correct: ___________________________________________
                              <br>
                              Director IV/Head of Department or Office
                              <br>
                              (Signature over Printed Name)
                              <br><br>
                              DATE:_______________________________________
                           </div>
                        </div>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>   
      </div>
   </body>
</html>
