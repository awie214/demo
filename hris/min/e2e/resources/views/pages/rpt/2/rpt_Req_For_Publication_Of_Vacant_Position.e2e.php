<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="9" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <?php spacer(10); ?>                     
                     <u>__________NAME OF AGENCY___________</u>
                     <?php spacer(10); ?>
                  </th>
               </tr>  
               <tr>
               		<th colspan="9" class="text-left">
               			<b>To: CIVIL SERVICE COMMISSION (CSC)</b>
               		</th>
               </tr>
               <tr>
               		<th></th>
               		<th colspan="9" class="text-left">
               			This is to request the publication of the following vacant positions of (Name of Agency) in the CSC website:
               		</th>
               </tr>
               <tr>
               		<th colspan="7"></th>
               		<th colspan="3" colspan="text-center">
               			______________________________________
               			<br>
               			(Head Of Agency)
               		</th>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">Position Title</th>
                  <th rowspan="2">Plantilla Item No.</th>
                  <th rowspan="2">Salary / Job / Pay Grade</th>
                  <th colspan="5">Qualification Standards</th>
                  <th rowspan="2">Place of Assignment</th>
               </tr>
               <tr class="colHEADER">
               		<th>Education</th>
               		<th>Training</th>
               		<th>Experience</th>
               		<th>Eligibility</th>
               		<th>Competency<br>(if applicable)</th>
               </tr>
            </thead>
            <tbody>
               <?php for ($j=1;$j<=10;$j++) { ?>
                  <tr>
                     <td>&nbsp;</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
               <?php } ?>
               <tr>
               		<td colspan="9">
               			<div class="row">
           					<div class="col-xs-12">
           						Interested and qualified applicants should signify their interest in writing. Attach the following documents to the application letter and send to the address below not later than ___________.
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-11">
           						1. Fully accomplished Personal Data Sheet (PDS) with recent passport-sized picture (CS Form No. 212, Revised 2017) which can be downloaded at www.csc.gov.ph;	
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-11">
           						2. Performance rating  in the present position for one (1) year (if applicable);
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-11">
           						3. Photocopy of certificate of eligibility/rating/license;
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-11">
           						4. Photocopy of Transcript of Records.
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-12">
           						<b>QUALIFIED APPLICANTS</b> are advised to hand in or send through courier/email their application to:
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-3">
           						<u>(Head of Office/Agency)</u>
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-3">
           						<u>(Position Title)</u>
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-3">
           						<u>(Complete Office Address)</u>
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-1"></div>
           					<div class="col-xs-3">
           						<u>(E-mail Address)</u>
           					</div>
               			</div>
               			<div class="row margin-top">
           					<div class="col-xs-12">
           						<b>APPLICATIONS WITH INCOMPLETE DOCUMENTS SHALL NOT BE ENTERTAINED.</b>
           					</div>
               			</div>
               		</td>
               </tr>
            </tbody>
         </table>
         <?php spacer(50); ?>
      </div>
   </body>
</html>
