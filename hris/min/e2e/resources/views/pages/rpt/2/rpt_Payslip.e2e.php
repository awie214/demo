<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         body, .rptBody {
            font-size: 9pt;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php rptHeader(getRptName(getvalue("drpReportKind"))); ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
         <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
               <div class="row">
                  <div class="col-xs-3">
                     <div class="row">
                        <label>Name of Employee</label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Employee No.
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Position
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           Office
                        </label>
                     </div>
                  </div>
                  <div class="col-xs-1">
                     <div class="row"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                     <div class="row margin-top"><label>:</label></div>
                  </div>
                  <div class="col-xs-8">
                     <?php
                        $rs = mysqli_query($conn,"SELECT * FROM empinformation LIMIT 1");
                        if (mysqli_num_rows($rs) > 0){
                           while ($row = mysqli_fetch_assoc($rs)){


                     ?>
                     <div class="row">
                        <label>
                        <?php
                           $refid = $row["EmployeesRefId"];
                           $result = mysqli_query($conn,"SELECT * FROM employees WHERE RefId = $refid");
                           if (mysqli_num_rows($result) > 0){
                              while ($emp = mysqli_fetch_assoc($result)){
                                 echo $emp["LastName"].", ".$emp["FirstName"];
                              }
                           }
                        ?>
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           <?php 
                              echo $row["PositionRefId"]; 
                           ?>
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           <?php
                              echo getRecord("position",$row["PositionRefId"],"Name");
                           ?>
                        </label>
                     </div>
                     <div class="row margin-top">
                        <label>
                           <?php
                              echo getRecord("office",$row["OfficeRefId"],"Name");
                           ?>
                        </label>
                     </div>
                     <?php
                           }
                        }
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 text-center">
                     <label>Monthly</label>
                  </div>
                  <div class="col-xs-3 text-center">
                     <label>Total</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     *EARNINGS*
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     Basic Salary
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     Personal Economic Relief Allowance
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <label>Mandatory Deductions</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     BIR Withholding Tax
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     GSIS Contribution
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     HDMF Contribution
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     PHIC Contribution
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3">
                     <label>Other Deductions:</label>
                  </div>
                  <div class="col-xs-6"></div>
                  <div class="col-xs-3">
                     <label>Accumulated Payment</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     GSIS Conso Loan
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     GSIS Policy Loan
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     GSIS EL/Calamity
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     GSIS Optional Life
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     GSIS Educ Loan
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     HDMF MPL
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3"></div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     HDMF MP2
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>0.00</label>
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>P 0.00</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3" style="padding-left: 4%;">
                     <b>NETPAY</b>
                  </div>
                  <div class="col-xs-3 text-right"></div>
                  <div class="col-xs-3 text-right">
                     <b>P 0.00</b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-center">
                     <label>*ACCUMULATED TOTALS*</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     Gross Compensation Income
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     Income Tax Withheld
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     GSIS Contribution
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     HDMF Contribution
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     PHIC Contribution
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-center">
                     <label>*OTHER COMPENSATION*</label>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     Communications Allow
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     Representation Allow
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     Transportation Allow
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-3"></div>
                  <div class="col-xs-6 text-right">
                     EME
                  </div>
                  <div class="col-xs-3 text-right">
                     P 0.00
                  </div>
               </div>
            </div>
         </div>
         <?php spacer(30); ?>
         <div class="row margin-top">
            <div class="col-xs-6">
               <i>This is a computer generated document and does not require any signature if without alterations</i>
            </div>
         </div>
      </div>
   </body>
</html>