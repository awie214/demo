<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="17" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>   
               <tr class="colHEADER">
                  <th rowspan ="2">ID</th>
                  <th rowspan ="2">EMPLOYEE NAME<br>NAME OF BENEFECIARY / <br> RELATIONSHIP</th>
                  <th rowspan ="2">DATE APPLIED</th>
                  <th rowspan ="2">MONTHLY<br>RATE / JG</th>
                  <th rowspan ="2">NO OF LEAVE<br>CREDITS<br>REQUESTED</th>
                  <th colspan="3">EARNED LEAVE<br>BEFORE MONETIZATION</th>
                  <th rowspan="2">&nbsp;</th>
                  <th colspan="3">AUTHORIZED LEAVE<br>TO BE MONETIZED</th>
                  <th rowspan="2">&nbsp;</th>
                  <th colspan="3">EARNED LEAVE<br>AFTER MONETIZATION</th>
                  <th rowspan="2">MONETIZED AMOUNT</th>
               </tr>
               <tr class="colHEADER">
                  <th>VL</th>
                  <th>SL</th>
                  <th>TOTAL</th>
                  <th>VL</th>
                  <th>SL</th>
                  <th>TOTAL</th>
                  <th>VL</th>
                  <th>SL</th>
                  <th>TOTAL</th>
               </tr>
            </thead>
            <tbody>
                  <?php for ($j=1;$j<=60;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>      
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="17">
                     <br><br>
                     <p>
                        <div class="row">
                           <div class="col-xs-4">
                               Certified Correct:
                           </div>
                           <div class="col-xs-4">
                              Approved for Payment:
                           </div>
                           <div class="col-xs-4">
                              Certified: Supporting documents complete and proper, and cash available in the amount of Php _______________.
                           </div>
                        </div>
                        <?php spacer(20); ?>
                        <div class="row">
                           <div class="col-xs-4">
                              <label>ANTONIA LYNNELY L. BAUTISTA</label><br>
                              Chief Admin Officer, HRDD
                           </div>
                           <div class="col-xs-4">
                              <label>GWEN GRECIA-DE VERA</label><br>
                              Executive Director
                           </div>
                           <div class="col-xs-4">
                              <label>CAROLYN V. AQUINO</label><br>
                              Accountant III, FPMO
                           </div>
                        </div>
                     </p>     
                  </td>
               </tr>
               <tr>
                  <td colspan="17">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>