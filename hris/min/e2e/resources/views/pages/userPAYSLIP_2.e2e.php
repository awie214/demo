<?php
   include_once 'constant.e2e.php';
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'PMSFunctions.e2e.php';
   include_once 'conn.e2e.php';
   include_once 'inc/inc_pms_payslip.e2e.php';
?>
<!DOCTYPE>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
	<script type="text/javascript">
      $(document).ready(function () {
         $("#btnPrint").click(function () {
            var head = $("head").html();
            printDiv('div_CONTENT',head);
         });
      });
   </script>
</head>
<body onload = "indicateActiveModules();">
   <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
      <?php $sys->SysHdr($sys,"pis"); ?>
      <div class="container-fluid" id="mainScreen">
         <?php doTitleBar ("PAYROLL"); ?>
         <div class="container-fluid margin-top">
            <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
            <div class="row">
               <div class="col-xs-10" id="div_CONTENT">
                  <div class="container-fluid rptBody">
                     <div class="row">
                     	<div class="col-xs-12">
                     		<?php rptHeader("EMPLOYEE PAYSLIP"); ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                     	<div class="col-xs-12">
                     		Employee Name: <?php echo $FullName; ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           Employee No: <?php echo $AgencyId; ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                     	<div class="col-xs-12">
                     		Position: <?php echo $Position; ?>
                     	</div>
                     </div>
                     <div class="row margin-top">
                     	<div class="col-xs-12">
                     		Office: <?php echo $Office; ?>
                     	</div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3 text-right">Monthly</div>
                        <div class="col-xs-3 text-right">Total</div>
                        <div class="col-xs-3"></div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3"><b>*EARNINGS*</b></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">Basic Salary</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($BASIC,2); ?>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">Personal Economic Relief Allowance</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($PERA,2); ?>
                        </div>
                        <div class="col-xs-3 text-left">
                           <?php
                              $new_salary = $BASIC + $PERA; 
                              echo number_format($new_salary,2); 
                           ?>
                        </div>
                        <div class="col-xs-3"></div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <b>Mandatory Deductions</b>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">BIR Withholding Tax</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($tax_contribution,2); ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">GSIS Contribution</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($gsis_contribution,2); ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">HDMF Contribution</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($pagibig_contribution,2); ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">PHIC Contribution</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($philhealth_contribution,2); ?>
                        </div>
                        <div class="col-xs-3 text-left">
                           <?php
                              $contribution_total = $philhealth_contribution + $tax_contribution + $gsis_contribution + $pagibig_contribution; 
                              echo number_format($contribution_total,2); 
                           ?>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           <b>Other Deductions</b>
                        </div>
                        <div class="col-xs-6 text-right">
                           <b>Accumulated Payment</b>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">GSIS Conso Loan</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($gsis_loan,2); ?>
                        </div>
                        <div class="col-xs-6 text-right">
                           P 0.00                           
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">GSIS Policy Loan</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($policy,2); ?>
                        </div>
                        <div class="col-xs-6 text-right">
                           P 0.00                           
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">GSIS EL/Calamity</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($EL_loan,2); ?>
                        </div>
                        <div class="col-xs-6 text-right">
                           P 0.00                           
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">Optional Life</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($Optional_loan,2); ?>
                        </div>
                        <div class="col-xs-6 text-right">
                           P 0.00                           
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">Education Loan</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($Educ_loan,2); ?>
                        </div>
                        <div class="col-xs-6 text-right">
                           P 0.00                           
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">HDMF MPL</div>
                        <div class="col-xs-3 text-left">
                           <?php echo number_format($pagibig_mpl,2); ?>
                        </div>
                        <div class="col-xs-6 text-right">
                           P 0.00                           
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3">HDMF MP2</div>
                        <div class="col-xs-3">
                           0.00
                        </div>
                        <div class="col-xs-3 text-right">
                           <?php
                              $deduction_total = $gsis_loan + $policy + $EL_loan + $Optional_loan + $Educ_loan + $pagibig_mpl;
                              echo number_format($deduction_total,2);
                           ?>                
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-3"><b>NET PAY</b></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3 text-right">
                           <?php
                              $grand_total = $new_salary - $contribution_total - $deduction_total;
                              echo number_format($grand_total,2);
                           ?>                
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-6">
                           <b>*OTHER COMPENSATION*</b>                
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-3">
                           Communication Allowance
                        </div>
                        <div class="col-xs-3 text-right">
                           P 0.00
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-3">
                           Representation Allowance
                        </div>
                        <div class="col-xs-3 text-right">
                           P 0.00
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-3">
                           Transportation Allowance
                        </div>
                        <div class="col-xs-3 text-right">
                           P 0.00
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-3">
                           EME
                        </div>
                        <div class="col-xs-3 text-right">
                           P 0.00
                        </div>
                     </div>
                     <br>
                     <br>
                     <qoute>
                     	This is a computer generated document and does not require any signature if without alterations
                     </qoute>
                  </div>
               </div>
            </div>
         </div>
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
      </div>
   </form>
</body>
</html>