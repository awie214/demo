<?php
   function insertIconLoans() {
      echo
      '<a href="javascript:void(0);">
         <i class="fa fa-plus-square" aria-hidden="true" id="bonusInsert" title="INSERT NEW" style="color:white;"></i>
      </a>';
   }
   function dobtnLoans() {
      echo
      '<hr>
      <div class="row">
         <div class="col-xs-12 txt-center">';
               createButton("Save","btnLocSaveItem","btn-cls4-sea","fa-floppy-o","");
               createButton("Cancel","btnLocCancelItem","btn-cls4-red","fa-undo","");
         echo
         '</div>
      </div>';
   }

?>
<script>
   $(document).ready(function () {
      $("#bonusInsert").click(function () {
         $("#hmode").val("ADD");
         $(".saveFields--").val("");
         $("#LoansTableSet").modal();
      });
   });
</script>
<div class="mypanel">
   <div class="row margin-top" id="newBenefits">
      <div class="col-xs-12">
         <div class="panel-top">
            <?php insertIconLoans(); ?> <label>INSERT NEW LOAN</label>
         </div> 
         <div class="panel-mid">
            <?php
               $table = "employees";
               $tableHdr = ["Loan Name","Total Loan Amount","Total Loan Balance","Amortization","Date Start","Date End"];
               $tableFld = ["","","","","",""];
               $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
               $action = ["true","true","true"];
               doGridTable($table,
                           $tableHdr,
                           $tableFld,
                           $sql,
                           $action,
                           "Deduction");
            ?>
         </div>
         <div class="panel-bottom"></div>
      </div>
   </div>
</div>

<!--modal-->
<div class="modal fade modalFieldEntry--" id="LoansTableSet" role="dialog">
   <div class="modal-dialog" style="width:75%;">
      <div class="mypanel" style="height:100%;">
         <div class="panel-top bgSea">
            <span id="modalTitle" style="font-size:11pt;">NEW BENEFITS AND ALLOWANCE INFORMATION</span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="panel-mid">
            <div class="mypanel">
               <div class="row" style="padding:10px;">
                  <div class="col-xs-12">
                     <div class="row">
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Loan Name:</label>
                              </div>
                              <div class="col-xs-6">
                                 <select class="form-input" name="LoanName">
                                    <option></option>
                                    <option></option>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Date Granted:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" name="DateGranted" class="form-input date--">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Total Loan Amount:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" name="TotalLoanAmount" class="form-input">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Total Loan Balance:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" name="TotalLoanBalance" class="form-input">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Amortization:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" name="Amortization" class="form-input">
                              </div>
                              <div class="col-xs-3 label">
                                 <input type="checkbox" name="Terminated">&nbsp;<label>Terminated</label>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Pay Rate:</label>
                              </div>
                              <div class="col-xs-6">
                                 <select class="form-input" name="LIPayRate">
                                    <option></option>
                                    <option></option>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Pay Period:</label>
                              </div>
                              <div class="col-xs-6">
                                 <select class="form-input" name="LIPayPeriod">
                                    <option></option>
                                    <option></option>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Date Start:</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="text" name="DateStart" class="form-input date--">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Date End:</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="text" name="DateEnd" class="form-input date--">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6 label">
                                 <label>Date Terminated:</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="text" name="DateTerminated" class="form-input date--">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="row margin-top" style="border:1px solid white;padding:10px;">
                           <?php bar();?>
                           <div class="row">
                              <div class="col-xs-12">
                                 <input type="checkbox" name="chkHoldPeriod">&nbsp;<label>Hold Period</label>
                              </div>
                           
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-6">
                                 <div class="row margin-top">
                                    <div class="col-xs-6 label">
                                       <label>Date From:</label>
                                    </div>
                                    <div class="col-xs-3">
                                       <input type="text" name="DateFrom" class="form-input date--">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="row margin-top">
                                    <div class="col-xs-6 label">
                                       <label>Date To:</label>
                                    </div>
                                    <div class="col-xs-3">
                                       <input type="text" name="DateTo" class="form-input date--">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php dobtnLoans(); ?>
                  </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>








