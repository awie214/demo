<?php
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   function dispNumFormat($val) {
      if ($val == "") {
         echo "0.00";
      } else {
         echo number_format($val,2);
      }
   }

   function dispBlank($val) {
      if ($val == "") {
         echo "---";
      } else {
         echo $val;
      }
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis") ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("My Service Record"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-sm-10" id="div_CONTENT">
                        <?php
                           $row = FindFirst("employees","WHERE RefId = $EmployeesRefId","*");
                           $childCount = 0;
                           $empRefid = $row['RefId'];
                           $sql = "SELECT * FROM `employeesmovement` WHERE CompanyRefId = $CompanyId ";
                           $sql .= "AND BranchRefId = $BranchId ";
                           $sql .= "AND EmployeesRefId = $empRefid ORDER BY EffectivityDate DESC";
                           $rsServiceRecords = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                           $numrow = mysqli_num_rows($rsServiceRecords);
                           if ($numrow > 0)
                           {
                              $EmployeesName = $row["FirstName"]." ".$row["MiddleName"].". ".$row["LastName"];
                              rptHeader("SERVICE RECORD");
                              include "inc/incServiceRecords.e2e.php";
                        ?>
                                 

                           <?php
                           } else {
                              echo "<h2>NO RECORD FOUND !!!</h2>";
                           }
                           ?>


                  </div>
               </div>
            </div>
            <?php
               //footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



