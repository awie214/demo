<?php
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'SysFunctions.e2e.php';
   $sys = new SysFunctions();

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {

         });
         function afterNewSave(LastRefId) {
            alert("New Record " + LastRefId + " Succesfully Inserted");
            gotoscrn ("pmsSpecialPayroll", setAddURL());
            return false;
         }
         function afterDelete() {
            alert("Record Succesfully Deleted");
            gotoscrn ("pmsSpecialPayroll", setAddURL());
            return false;
         }
         function afterEditSave(RefId){
            alert("Record " + RefId + " Succesfully Updated");
            gotoscrn ("pmsSpecialPayroll", setAddURL());
            return false;
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("SPECIAL PAYROLL"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div id="divList">
                        <div class="mypanel">
                           <div class="panel-top">LIST</div>
                           <div class="panel-mid">
                              <span id="spGridTable">
                                 <?php
                                 $table = "employeesspecialpayroll";
                                 $tableHdr = ["Employees ID","Employee Name","Netpay"];
                                 $tableFld = ["EmployeesId","EmployeesRefId","Netpay"];
                                 $sql = "SELECT * FROM $table";
                                 doGridTable($table,
                                             $tableHdr,
                                             $tableFld,
                                             $sql,
                                             ["true","true","true"],
                                             "gridTable");
                                 ?>
                              </span>
                              <?php
                                    btnINRECLO([true,true,false]);
                              ?>
                           </div>
                           <div class="panel-bottom"></div>
                        </div>
                     </div>
                     <div id="divView">
                        <div id="divView">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">INSERTING NEW SPECIAL PAYROLL</span>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <label>Employees ID:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <input type="text" class="form-input saveFields--" name="char_EmployeesId">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <label>Benefits:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <?php
                                                   //createSelect("benefits","sint_BenefitRefId","","80","","SELECT BENEFIT","false")
                                                   $ftable = "benefits";
                                                   createSelect($ftable,
                                                                "sint_BenefitRefId",
                                                                "",100,"Name","Select ".$ftable,"");
                                                ?>
                                             </div>
                                             <div class="col-xs-3 txt-right">
                                                <label>Deduction:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <?php
                                                   $ftable = "deductions";
                                                   createSelect($ftable,
                                                                "sint_DeductionRefId",
                                                                "",100,"Name","Select ".$ftable,"");
                                                ?>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <label>Tax Rate:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <input type="text" class="form-input saveFields--" name="deci_TaxRate">
                                             </div>
                                             <div class="col-xs-3 txt-right">
                                                <label>Tax Amount:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <input type="text" class="form-input saveFields--" name="deci_TaxAmount">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <label>Netpay:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <input type="text" class="form-input saveFields--" name="deci_Netpay">
                                             </div>
                                          </div>
                                          <div class="row margin-top" style="padding-left: 10px;">
                                             <?php btnSACABA("true","true","true");?>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>



