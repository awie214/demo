$(document).ready(function () {
   $("[name='btnTab_1']").click(function () {
      tabClick(1);
   });
   $("[name='btnTab_2']").click(function () {
      tabClick(2);
   }); 
   $("[name='btnTab_3']").click(function () {
      tabClick(3);
   });
   $("[name='btnTab_4']").click(function () {
      tabClick(4);
   });
   $("[name='btnTab_5']").click(function () {
      tabClick(5);
   });
   $("[name='btnTab_6']").click(function () {
      tabClick(6);
   });
   $("[name='btnTab_7']").click(function () {
      tabClick(7);
   });
   $("[name='btnUPDATE']").click(function () {
      if ($("[name='hRefIdSelected']").val() > 0)  {
         $.ajax({
            url: "pmsTrn.e2e.php",
            type: "POST",
            data: new FormData($("[name='xForm']")[0]),
            success: function (responseTxt) {
               //$.notify(responseTxt);
               alert(responseTxt);
            },
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false
         });
      } else {
         $.notify("No Employees Selected", "info");
         return false;
      }
   });
   $("[class*='saveFields--']").prop("disabled", true);
   $("[class*='enabler--']").each(function () {
      $(this).click(function () {
         var panel = $(this).attr("for");
         if (panel != undefined) {
            if ($(this).is(":checked")) {
               $("#" + panel + " [class*='saveFields--']").prop("disabled", false);
            } else {
               $("#" + panel + " [class*='saveFields--']").prop("disabled", true);
            }
         }
         showBtnUpd();
      });
   });

   // on load;
   $(function () {  
      showBtnUpd();
   });
   
   /*$("[name*='sint_'], [name*='deci_']").each(function () {
      $(this).addClass("number--");
   });*/
});

function selectMe(emprefid) {
   $.get(
      "EmpQuery.e2e.php",
      {
         emprefid: emprefid,
         hCompanyID: $("#hCompanyID").val(),
         hBranchID: $("#hBranchID").val(),
         hEmpRefId: $("#hEmpRefId").val(),
         hUserRefId: $("#hUserRefId").val()
      },
      function (data, status) {
         $("[id*='tab_']").hide();
         setValueById("hRefIdSelected", 0); /* important */
         setHTMLById("lblRefIdSelected", "&nbsp;");
         setHTMLById("lblEmpLastName", "&nbsp;");
         setHTMLById("lblEmpFirstName", "&nbsp;");
         setHTMLById("lblEmpMiddleName", "&nbsp;");
         setHTMLById("lblEmpPosition", "&nbsp;");
         if (status == "error") {
            $.notify("System Encounter Error !!! fn(selectMe)", "warn");
            return false;
         }
         if (status == "success") {
            if (data != "") {
               var data = JSON.parse(data);
               try {
                  setValueById("hRefIdSelected", data.RefId);
                  setHTMLById("lblRefIdSelected", data.RefId);
                  setHTMLById("lblEmpLastName", data.LastName);
                  setHTMLById("lblEmpFirstName", data.FirstName);
                  setHTMLById("lblEmpMiddleName", data.MiddleName);
                  setHTMLById("lblEmpPosition", "&nbsp;");
                  /*Code Here Employment Summary*/
                  $.get(
                     "pmsTrn.e2e.php",
                     {
                        emprefid: emprefid,
                        fn: "getEmployeesPMSRecord",
                        json:"pmsEmployee",
                        hCompanyID: $("#hCompanyID").val(),
                        hBranchID: $("#hBranchID").val(),
                        hEmpRefId: $("#hEmpRefId").val(),
                        hUserRefId: $("#hUserRefId").val()
                     },
                     function (data, status) {
                        if (status == "error") {
                           $.notify("System Encounter Error !!! fn(getEmployeesPMSRecord)", "warn");
                           return false;
                        }
                        if (status == "success") {
                           //alert(data);
                           if (data != "") {
                              try {
                                 eval(data);
                              } catch (e) {
                                 if (e instanceof SyntaxError) {
                                    alert(e.message);
                                 }
                              }
                           } else {
                              $.notify("[pms] No Data Returned in Employees Ref. ID: " + emprefid, "info");
                              return false;
                           }
                        }
                     }
                  );
               }
               catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            } else {
               $.notify("[pms] No Returned Data in Employees Ref. ID: " + emprefid, "info");
               return false;
            }
         }
      }
   );
}